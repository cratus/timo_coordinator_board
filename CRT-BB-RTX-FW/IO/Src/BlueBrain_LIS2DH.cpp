/*
$DESCRIPTION		: This file contains definitions for LIS2DH  functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_LIS2DH.h"

LIS2DH::LIS2DH() 
{
    _address = LIS2DH_DEFAULT_ADDRESS;
		filter_length = 5;
}

bool
LIS2DH::init(void) 
{
    _address = LIS2DH_DEFAULT_ADDRESS; 
		uint8_t status = HAL_I2C_IsDeviceReady(&hi2c1,_address,10,100);
		if(status != HAL_OK) {
			return false;
		}
		if(!whoAmI()) {
			return false;
		}
		
    //Put into the correct operating mode 
		if(!setMode(LIS2DH_NORMAL))
		{
			return false;
		}
    return setDataRate(LIS2DH_ODR_400Hz);
		
		// Already initialized in BlueBrain_init()
}

bool 
LIS2DH::setHighResolutionMode(void) {
		writeMaskedRegister(LIS2DH_CTRL_REG4, LIS2DH_HR_MASK, true);
		return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_LPEN_MASK, false);
}

bool 
LIS2DH::disableHighResolutionMode(void) {
		
	return writeMaskedRegister(LIS2DH_CTRL_REG4, LIS2DH_HR_MASK, false);
		
}

bool 
LIS2DH::enablePowerDownMode(void) {
	return setDataRate(LIS2DH_ODR_0Hz);	
	//return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_ODR_MASK, 0);
}

bool LIS2DH::setMode(uint8_t mode) {

	switch(mode)
	{
		case LIS2DH_POWER_DOWN:
			disableHighResolutionMode();
			disableAxisXYZ();
			disableLowPower();
			enablePowerDownMode();
		break;
		
		case LIS2DH_LOW_POWER:
			disableHighResolutionMode();
			enableLowPower();	
			setDataRate(LIS2DH_ODR_100Hz);
			enableAxisXYZ();
			shift_param = 8;
		break;
		
		case LIS2DH_NORMAL:
			disableLowPower();
			disableHighResolutionMode();
			setDataRate(LIS2DH_ODR_100Hz);
			enableAxisXYZ();
			shift_param = 6;
		break;
		
		case LIS2DH_HIGH_RESOLUTION:
			disableLowPower();
			setHighResolutionMode();
			setDataRate(LIS2DH_ODR_100Hz);
			shift_param = 4;
		break;
	}
	
	return true;
	
}

bool 
LIS2DH::selectScale(LIS2DH_Fullscale_t scale)
{
		return writeMaskedRegister(LIS2DH_CTRL_REG4, (scale & LIS2DH_FS_MASK),true);
}

bool
LIS2DH::writeRegister(const uint8_t register_addr, const uint8_t value) 
{
    //send write call to sensor address
    //send register address to sensor
    //send value to register

		uint8_t data_to_send = value;
	
		uint8_t status = HAL_I2C_Mem_Write(&hi2c1, _address,(uint16_t)register_addr,1,&data_to_send,1,100);
		if(status == HAL_OK) {
			return true;
		}
		else {
			return false;   	//returns whether the write succeeded or failed
		}
}

uint8_t 
LIS2DH :: readRegister(const uint8_t register_addr) 
{
    //send write call to sensor address
    //send register address to sensor
    //send value to register
  
		uint8_t value = 0;
		uint8_t status = HAL_I2C_Mem_Read(&hi2c1, _address,(uint16_t)register_addr,1,&value,1,100);
		if(status==HAL_OK) {
			return value; //returns whether the write succeeded or failed
		}
		else {
			return 0;
		}
}

bool
LIS2DH::writeRegisters(const uint8_t msb_register, const uint8_t msb_value, const uint8_t lsb_register, const uint8_t lsb_value) 
{ 
    //send write call to sensor address
    //send register address to sensor
    //send value to register

		bool msb_bool, lsb_bool;
    msb_bool = writeRegister(msb_register, msb_value);
    lsb_bool = writeRegister(lsb_register, lsb_value);
    return (msb_bool | lsb_bool); //returns whether the write succeeded or failed
}

bool
LIS2DH::writeMaskedRegister(const uint8_t register_addr, const uint8_t mask, const bool value) 
{
    uint8_t data = readRegister(register_addr);
    uint8_t combo;
    if(value) {
        combo = (mask | data);
    } else {
        combo = ((~mask) & data);
    }
    return writeRegister(register_addr, combo);
}

int16_t
LIS2DH::readRegisters(const uint8_t msb_register, const uint8_t lsb_register) 
{
    uint8_t msb = readRegister(msb_register);
    uint8_t lsb = readRegister(lsb_register);
    return ((((int16_t)msb) << 8) | lsb);
}

uint8_t 
LIS2DH::readMaskedRegister(const uint8_t register_addr, const uint8_t mask) 
{
    uint8_t data = readRegister(register_addr);
    return (data & mask);
}

/** Read the X axis registers
 * @see LIS2DH_OUT_X_H
 * @see LIS2DH_OUT_X_L
 */
int16_t 
LIS2DH::getAxisX(void) 
{
		return (readRegisters(LIS2DH_OUT_X_H, LIS2DH_OUT_X_L));
}


/** Read the Y axis registers
 * @see LIS2DH_OUT_Y_H
 * @see LIS2DH_OUT_Y_L
 */
int16_t
LIS2DH::getAxisY(void) 
{
		return ~(readRegisters(LIS2DH_OUT_Y_H, LIS2DH_OUT_Y_L));
}

/** Read the Z axis registers
 * @see LIS2DH_OUT_Z_H
 * @see LIS2DH_OUT_Z_L
 */
int16_t 
LIS2DH::getAxisZ(void) 
{
		return readRegisters(LIS2DH_OUT_Z_H, LIS2DH_OUT_Z_L);
}

/** Read the all axis registers
 * @see getAxisZ()
 * @see getAxisY()
 * @see getAxisZ()
 */
void
LIS2DH::getMotion(int16_t* ax, int16_t* ay, int16_t* az) 
{
    *ax = getAxisX();
    *ay = getAxisY();
    *az = getAxisZ();
}

bool
LIS2DH::tempHasOverrun(void) 
{
    uint8_t overrun = readMaskedRegister(LIS2DH_STATUS_REG_AUX, LIS2DH_TOR_MASK);
    return (overrun != 0);
}

bool
LIS2DH::tempDataAvailable(void) 
{
    uint8_t data = readMaskedRegister(LIS2DH_STATUS_REG_AUX, LIS2DH_TDA_MASK);
    return (data != 0);
}

uint16_t 
LIS2DH::getTemperature(void) 
{
    if(tempDataAvailable()||1){
        return readRegisters(LIS2DH_OUT_TEMP_H, LIS2DH_OUT_TEMP_L);
    } else {
        //if new data isn't available
        return 0;
    }
}

bool
LIS2DH::whoAmI(void) 
{
    return (LIS2DH_I_AM_MASK == readRegister(LIS2DH_WHO_AM_I));
}

bool
LIS2DH::getTempEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_TEMP_CFG_REG, LIS2DH_TEMP_EN_MASK) != 0);
}

bool
LIS2DH::setTempEnabled(bool enable) 
{
    return writeRegister(LIS2DH_TEMP_CFG_REG, (enable ? LIS2DH_TEMP_EN_MASK : 0));
}

uint8_t
LIS2DH::getDataRate(void) 
{
    return readMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_ODR_MASK);
}

bool
LIS2DH::setDataRate(uint8_t rate) 
{
    if(rate > 9) {
        return false;
    }
    uint8_t data_rate = (rate << 4);
		data_rate|=readMaskedRegister(LIS2DH_CTRL_REG1,LIS2DH_XYZ_EN_MASK);
		data_rate|=readMaskedRegister(LIS2DH_CTRL_REG1,LIS2DH_LPEN_MASK);
    return writeRegister(LIS2DH_CTRL_REG1, data_rate);
}

bool
LIS2DH::enableLowPower(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_LPEN_MASK, true);
}


bool
LIS2DH::disableLowPower(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_LPEN_MASK, false);
}


bool
LIS2DH::isLowPowerEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_LPEN_MASK) != 0);
}

bool
LIS2DH::enableAxisX(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_X_EN_MASK, true);
}

bool
LIS2DH::disableAxisX(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_X_EN_MASK, false);
}

bool
LIS2DH::isXAxisEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_X_EN_MASK) != 0);
}

bool
LIS2DH::enableAxisY(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Y_EN_MASK, true);
}

bool
LIS2DH::disableAxisY(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Y_EN_MASK, false);
}

bool
LIS2DH::isYAxisEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Y_EN_MASK) != 0);
}

bool
LIS2DH::enableAxisZ(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Z_EN_MASK, true);
}

bool
LIS2DH::disableAxisZ(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Z_EN_MASK, false);
}

bool
LIS2DH::isZAxisEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_Z_EN_MASK) != 0);
}

bool
LIS2DH::enableAxisXYZ(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_XYZ_EN_MASK, true);
}

bool
LIS2DH::disableAxisXYZ(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG1, LIS2DH_XYZ_EN_MASK, false);
}

bool 
LIS2DH::getHPFilterMode(uint8_t mode) 
{
    return readMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPM_MASK);
}

bool
LIS2DH::setHPFilterMode(uint8_t mode) 
{
    if(mode > 3) {
        return 0;
    }
    uint8_t filter_mode = mode << 6;
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPM_MASK, filter_mode);
}

//FDS functions

bool
LIS2DH::EnableHPClick(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPCLICK_MASK, true);
}

bool
LIS2DH::disableHPClick(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPCLICK_MASK, false);
}

bool
LIS2DH::isHPClickEnabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPCLICK_MASK) != 0);
}

bool
LIS2DH::EnableHPIS1(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS1_MASK, true);
}

bool
LIS2DH::disableHPIS1(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS1_MASK, false);
}

bool
LIS2DH::isHPIS1Enabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS1_MASK) != 0);
}

bool
LIS2DH::EnableHPIS2(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS2_MASK, true);
}

bool
LIS2DH::disableHPIS2(void) 
{
    return writeMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS2_MASK, false);
}

bool
LIS2DH::isHPIS2Enabled(void) 
{
    return (readMaskedRegister(LIS2DH_CTRL_REG2, LIS2DH_HPIS2_MASK) != 0);
}

bool
LIS2DH :: accl_lis2dh_read_xyz_accl_values(uint8_t* xyz)
{

	int16_t ax = getAxisX() >> 4;		//	High Resolution Mode - 12 bit
	int16_t ay = getAxisY() >> 4;		//	High Resolution Mode - 12 bit
	int16_t az = getAxisZ() >> 4;		//	High Resolution Mode - 12 bit
	
	xyz[0] = (uint8_t)(ax >> 8);
	xyz[1] = (uint8_t)(ax & 0x00FF);
	xyz[2] = (uint8_t)(ay >> 8);
	xyz[3] = (uint8_t)(ay & 0x00FF);
	xyz[4] = (uint8_t)(az >> 8);
	xyz[5] = (uint8_t)(az & 0x00FF);;
	
	return 1;
}

bool
LIS2DH :: getMotionData(int16_t* accel_x,int16_t* accel_y,int16_t* accel_z)
{
		static uint8_t k=0;
		uint8_t i=0;
		double buffer_accelx=0;
		double buffer_accely=0;
		double buffer_accelz=0;
	
		bufferx[k%5] = getAxisX()>>4;		//	High Resolution Mode - 12 bit
		buffery[k%5] = getAxisY()>>4;		//	High Resolution Mode - 12 bit
		bufferz[k%5] = getAxisZ()>>4;		//	High Resolution Mode - 12 bit
			
		for(i=0; i<5; i++) {
			buffer_accelx += bufferx[i];
			buffer_accely += buffery[i];
			buffer_accelz += bufferz[i];
		}
		
		*accel_x = buffer_accelx/filter_length;
		*accel_y = buffer_accely/filter_length;
		*accel_z = buffer_accelz/filter_length;
		k++;
		
		if(k>200) {
			k=0;
		}
		
		return true;
		
}
