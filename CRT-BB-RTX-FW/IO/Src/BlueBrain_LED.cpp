/*
$DESCRIPTION		: This file contains definitions for LED functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/ 

#include "BlueBrain_LED.h"
#include <stdbool.h>

bool
LED::init()
{
    /* Pins initialized by BlueBrain_init() */
    return true;
}

void
LED::on(uint8_t ledNum)
{
    mLedValue or_eq ledNum;
    setAll(mLedValue);
}
void
LED::off(uint8_t ledNum)
{
    mLedValue and_eq ~ledNum;
    setAll(mLedValue);
}
void
LED::toggle(uint8_t ledNum)
{
    mLedValue xor_eq ledNum;
    setAll(mLedValue);
}
void
LED::set(uint8_t ledNum, bool ON)
{
    ON ? on(ledNum) : off(ledNum);
}

void
LED::setAll(uint8_t value)
{
    /* LEDs are active low */
    #define led_set(num, realbit) 																								\
										do {          																								\
												if (mLedValue & (1 << num)) {             								\
														HAL_GPIO_WritePin(GPIOB,realbit,GPIO_PIN_SET); } 			\
												else {                                    								\
														HAL_GPIO_WritePin(GPIOB,realbit,GPIO_PIN_RESET); 			\
												}																													\
										} while (0)

		mLedValue = value & 0x0F;
    led_set(0, 1);
    led_set(1, 2);
    led_set(2, 4);
}

uint8_t 
LED::getValues(void) const
{
    return mLedValue;
}
