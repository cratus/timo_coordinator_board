/*
$DESCRIPTION		: This file contains definitions for LED functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_IO_LED_H
#define __BLUEBRAIN_IO_LED_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "singleton_template.h"
#include <stdint.h>

#define 	LED_black				0
#define 	LED_red					1
#define		LED_green				2
#define		LED_yellow			3
#define		LED_blue				4
#define		LED_purple			5
#define		LED_magenta			6
#define		LED_white				7
	
/**
	* LED class used to control the Board's 3 output LEDs
	*
	* @ingroup BoardIO
	*/
class LED : public SingletonTemplate<LED>
{
		public:
					bool init(); ///< Initializes this device, @returns true if successful
	
					void on(uint8_t ledNum);            ///< Turns  ON LED. @param ledNum The LED # from 0-7
					void off(uint8_t ledNum);           ///< Turns OFF LED. @param ledNum The LED # from 0-7
					void set(uint8_t ledNum, bool on);  ///< Turns on/off led based on @param on
					void toggle(uint8_t ledNum);        ///< Toggles the LED
					void setAll(uint8_t value);         ///< Sets 3-bit value of 3 pins
					uint8_t getValues(void) const;      ///< Get the LED bit values currently set

		private:
					uint8_t mLedValue; ///< Current bits set on the LEDs

					LED() : mLedValue (0) {}    ///< Private constructor of this Singleton class
					friend class SingletonTemplate<LED>;  ///< Friend class used for Singleton Template
};

#endif /*__BLUEBRAIN_IO_LED_H */
