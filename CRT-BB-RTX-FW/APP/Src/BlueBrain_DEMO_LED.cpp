/*
$DESCRIPTION		: This file contains APIs to easily initialize accelerometer sensor and 
									run the different LED patterns on the Bluebrain

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

#include "BlueBrain_DEMO_LED.h"
#include "BlueBrain_IO.h"


bool BB_LED_Demo :: init(void)
{
	LE.init();
	return true;
}


bool BB_LED_Demo :: run(void)
{
	LE.toggle(LED_red);
	delay(100);
	LE.toggle(LED_blue);
	delay(100);
	LE.toggle(LED_green);
	delay(100);
	return true;
}

void BB_LED_Demo :: demo_LED_Switch(void)
{
	// LE.on(LED_red);
	//GPIO_toggle(GPIO_0);
	//HAL_Delay(100);
	//GPIO_toggle(GPIO_1);
	//HAL_Delay(100);
	//GPIO_toggle(GPIO_2);
	//delay(100);
	GPIO_toggle(GPIO_3);
	delay(100);
	GPIO_toggle(GPIO_4);
	delay(100);
	GPIO_toggle(GPIO_5);
	delay(100);
	GPIO_toggle(GPIO_6);
	delay(100);
	GPIO_toggle(GPIO_7);
	delay(100);
	/*if (GPIO_read_pin(GPIO_0)) 
	{}//GPIO_set(GPIO_1);
	else
	{
	}
		//GPIO_reset(GPIO_1);
	*/
	delay(100);
}
