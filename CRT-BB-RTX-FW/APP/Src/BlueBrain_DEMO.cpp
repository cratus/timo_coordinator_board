/*
$DESCRIPTION		: This file contains definitions for BlueBrain_Demo class functions

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

#include "BlueBrain_DEMO.h"

bool BlueBrain_Demo :: free_memory(void)
{
	// Needs to implement in future is any Malloc operations are there. 
	return true;
}
