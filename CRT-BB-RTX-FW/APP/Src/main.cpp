/*
$DESCRIPTION		: This is the main file which contains calling application level functions like initialization and run

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17
                : (C) COPYRIGHT STMicroelectronics

$Project				: BLUEBRAIN

$Author					: David

*/

/* Includes ------------------------------------------------------------------*/
#include "TIMO.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>

void UserUartTask(void const *params);
osThreadId										        user_uart_task_id;												    /**<  User UART thread */
osThreadDef(UserUartTask, osPriorityHigh, 1, 0);                             		/**< Definition of User UART thread */

#ifdef TERMINAL_ENABLE
void TerminalTask(void const *params);
static osThreadId										terminal_task_id;														/**<  BB Terminal Task thread */
osThreadDef(TerminalTask, osPriorityNormal, 1, 0);                             	/**< Definition of Terminal Task thread */
#endif

#ifdef SYNC_TEST
void RTCMonTask(void const *params);
static osThreadId										rtc_mon_task_id;														/**<  RTC monitor task thread */
osThreadDef(RTCMonTask, osPriorityHigh, 1, 0);                             		/**< Definition of Terminal Task thread */
#endif

static char fw_rev[] = "\n\n\n\n\rTIMO 0.1 (proof-of-concept) - Coordinator Board\n";

int 
main(void) {
	blue_brain_system_init(); //Initialize the bluebrain micro-controller peripherals

	#ifdef TERMINAL_ENABLE
		terminal_task_id = osThreadCreate(osThread(TerminalTask), NULL);
		UNUSED_VARIABLE(terminal_task_id);
	#endif
	
	#ifdef SYNC_TEST
	rtc_mon_task_id = osThreadCreate(osThread(RTCMonTask), NULL);
	UNUSED_VARIABLE(rtc_mon_task_id);
	#endif
	
		/*
	 * TIMO - create user UART task
	 */
	user_uart_task_id = osThreadCreate(osThread(UserUartTask), NULL);
	
	// Start execution.
	osDelay(osWaitForever);
  		
	/* We should never get here as control is now taken by the scheduler */	
	while (1) {			/* Infinite loop */
		UNUSED_VARIABLE(osDelay(1000));
	}
	return -1;
}

/*
 * blue_brain_system_init - Function to initialize bluebrain system
 * 
 * Initializes all the peripherals of bluebrain board
 * 
*/


#define networkAddrReadGPIOfirst (GPIO_0)
#define networkAddrReadGPIOs     (GPIO_0|GPIO_1|GPIO_2|GPIO_3|GPIO_4)
#define rs485WriteGPIOs          (GPIO_5|GPIO_6|GPIO_7)


void 
blue_brain_system_init()
{
	BlueBrain_init();

	//GPIO_makeoutput(GPIO_0|GPIO_1|GPIO_2|GPIO_3|GPIO_4|GPIO_5|GPIO_6|GPIO_7);
	GPIO_makeoutput(rs485WriteGPIOs);      // set up RS-485 control lines (outputs)
  GPIO_makeinput (networkAddrReadGPIOs); // set up Network Address lines (inputs)
	GPIO_pullup    (networkAddrReadGPIOs); // pull up Network Address lines
	

	
	DEBUG_UART.init(UART_BAUD_115200,UART_PARITY_NONE,UART_BITS_8,UART_STOP_1);
	USER_UART.init (UART_BAUD_115200,UART_PARITY_NONE,UART_BITS_8,UART_STOP_1);	
	LE.on(0x07); // Turn off LEDs
	
	//
	// Identify this app and revision
	//
	printf(fw_rev);
}
