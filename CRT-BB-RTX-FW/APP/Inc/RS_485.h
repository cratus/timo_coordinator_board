/*
$DESCRIPTION		: This file contains definitions for RS-485 function for TIMO project

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: TIMO

$Author					: David

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RS_485_H
#define __RS_485_H

#define RS_485_NETWORK_MASK (0x1F)  // RS-485 network address - 00 - 31 (0x00 - 0x1F)
#define RS_485_NETWORK_ADDRESS_BIT_COUNT 5

/* Sensor Board */
#define RS_485_ROUND_TRIP   (0x20)  // RS-485 round-trip character (sent from Coordinator board to addressed Sensor Board)
#define RS_485_GET_TIME     (0x40)  // RS-485 get-time character (packet containing Sensor Board time sent to Coordinator Board)
#define RS_485_SET_TIME     (0x60)  // RS-485 set-time packet sent from Coordinator board to adressed Sensor Board
/* Coordinator Board */


typedef struct timoPacket
{
	uint8_t  address;
	uint8_t  event;
	uint32_t hms;
	uint16_t sub_seconds;
	uint16_t crc;
} __attribute__((packed)) TIMO_PACKET;
	 
void RS_485_sendAddressCharacter(uint8_t);
void RS_485_sendPacket(timoPacket pk);
void RS_485_sendChar(uint16_t ch);
int  RS_485_getChar(void);
#endif /*__RS_485_H */
