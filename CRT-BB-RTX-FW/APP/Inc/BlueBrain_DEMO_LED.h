/*
$DESCRIPTION		: This file contains LED demo class declaration.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_LED_DEMO_H
#define __BLUEBRAIN_APP_LED_DEMO_H

#include "BlueBrain_Demo.h"
#include "BlueBrain_IO.h"
#include "BlueBrain_GPIO.h"
#include "BlueBrain_utilities.h"

class BB_LED_Demo : public BlueBrain_Demo
{
	public:
		
		bool init(void);
	
		bool run(void);
	
		void demo_LED_Switch(void);
	

};



#endif /*__BLUEBRAIN_APP_LED_DEMO_H */