/*
$DESCRIPTION		: This file contains FFT demo class declaration.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project			: BLUEBRAIN

$Author				: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_FFT_DEMO_H
#define __BLUEBRAIN_APP_FFT_DEMO_H

#include "BlueBrain_Demo.h"
#include "BlueBrain_IO.h"
#include "BlueBrain_GPIO.h"
#include "BlueBrain_utilities.h"
#include "arm_math.h"

#define TEST_LENGTH_SAMPLES 2048 

class BB_FFT_Demo : public BlueBrain_Demo
{
	public:
		
		bool init(void);
	
		bool run(void);
	
		//void demo_LED_Switch(void);
	

};



#endif /*__BLUEBRAIN_APP_FFT_DEMO_H */