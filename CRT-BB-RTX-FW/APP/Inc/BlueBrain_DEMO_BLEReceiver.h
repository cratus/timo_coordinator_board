/*
$DESCRIPTION		: This file contains BLE Receiver demo class declaration.

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_BLERECEIVE_DEMO_H
#define __BLUEBRAIN_APP_BLERECEIVE_DEMO_H

#include "BlueBrain_Demo.h"
#include "BlueBrain_IO.h"
#include "BlueBrain_GPIO.h"
#include "BlueBrain_utilities.h"

class BB_BLEReceive_Demo : public BlueBrain_Demo
{
	public:
		
		bool init(void);
	
		bool run(void);
	
	private:
		
	uint8_t received_data;

};



#endif /*__BLUEBRAIN_APP_BLERECEIVE_DEMO_H */