/*
$DESCRIPTION		: This file contains Temperature demo class declaration.
									
$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_APP_TEMP_DEMO_H
#define __BLUEBRAIN_APP_TEMP_DEMO_H


#include "BlueBrain_SI7050.h"
#include "BlueBrain_Demo.h"

class BB_Temp_SensorDemo : public BlueBrain_Demo
{
	public:
		
		bool init(void);
	  
	  bool run(void);
	
	private:
		SI7050 Temp_Sensor;
};

#endif /*__BLUEBRAIN_APP_TEMP_DEMO_H */