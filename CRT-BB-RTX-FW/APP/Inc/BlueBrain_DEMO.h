/*
$DESCRIPTION		: This file contains general demo class which is giving idea to write any demo application

$Copyright			: cratustech.com - Copyright (C) CRATUS Technology, Inc. 2013-17

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_DEMO_H
#define __BLUEBRAIN_DEMO_H


/*
	 Those methods which needs to be over written are provided as pure virtual 
	 methods and methods which are common are normal method in this parent class. 
*/

class BlueBrain_Demo
{
	protected:
		
		virtual bool init(void) = 0;

		virtual bool run(void)  = 0;
	
		bool free_memory(void);
	
};	
	
#endif /*__BLUEBRAIN_DEMO_H */
