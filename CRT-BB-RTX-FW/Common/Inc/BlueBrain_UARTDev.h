/*
$DESCRIPTION		: This file contains definitions for UART functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Chitrang

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_UART_H
#define __BLUEBRAIN_HAL_UART_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stdbool.h"
#include "singleton_template.h"

class UartDev {
	
	public:
		HAL_StatusTypeDef print(const char *pData);
		HAL_StatusTypeDef print(unsigned int num);
		HAL_StatusTypeDef print(signed int lcd_num);
		HAL_StatusTypeDef println(uint8_t *pData);
		bool print_EOL(uint8_t EOL_setting);
		void print_echo(bool on);
		HAL_StatusTypeDef read(uint8_t *pData, uint16_t Size);
		bool readln(uint8_t *pData, uint16_t Size);
		UartDev(unsigned int* pUARTBaseAddr);
		~UartDev() { }
	
	protected:
		HAL_StatusTypeDef init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits);	
	
	private:
		
		UartDev();
		UART_HandleTypeDef* mUARTRegBase; // Pointer to UART memory map
};



#endif /*__BLUEBRAIN_HAL_UART_H */