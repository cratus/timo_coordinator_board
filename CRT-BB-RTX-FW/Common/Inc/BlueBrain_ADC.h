/*
$DESCRIPTION		: This file contains definitions for ADC functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_ADC_H
#define __BLUEBRAIN_HAL_ADC_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

	 
		
#define ADC_Channel0			0						//ADC1_IN6
#define ADC_Channel1			1						//ADC1_IN7
#define ADC_Temperature		2						//ADC_CHANNEL_TEMPSENSOR			//ADC1_IN16
#define ADC_Channel2			3						//ADC2_IN4
#define ADC_Channel3			4						//ADC2_IN5



#define ADC_CHANNEL_NO_3 	3
#define ADC_HADC1_LIMIT 	5
				
/* For below channels which ADC channel should be used is still not figured*/		
#define ADC_Battery				5						//ADC_CHANNEL_VBAT
#define ADC_Vref					6						//ADC_CHANNEL_VREFINT
		
/* ADC Databits Resolution :
 *		ADC_RESOLUTION_12B
 *		ADC_RESOLUTION_10B
 *		ADC_RESOLUTION_8B
 *		ADC_RESOLUTION_6B
*/
	
	
//ADC_config - Configure the ADC channel number with specified resolution
void 
ADC_config(uint8_t channel_no, uint32_t bitcount);

//ADC_read - Read the ADC data for the specified channel number
uint32_t 
ADC_read(uint8_t channel_no);

#endif /*__BLUEBRAIN_HAL_ADC_H */
