/*
$DESCRIPTION		: This file contains definition for Singleton Class

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SINGLETON_TEMPLATE_HPP
#define __SINGLETON_TEMPLATE_HPP

/**
	* There are 3 kinds of Singleton Instances to choose from:
	* - STATIC_INSTANCE
	*      Static instance is created inside the getInstance() which returns this static instance's address
	* - STATIC_POINTER_INSTANCE
	*      Static pointer is created which is initialized upon declaring it, and getInstance() returns this pointer.
	* - STATIC_PRIVATE_POINTER_INSTANCE
	*      Private Pointer is initialized to NULL as a member, and then if it is NULL, the class instance is created.
	*/

#define STATIC_INSTANCE                     1
#define STATIC_POINTER_INSTANCE             2
#define STATIC_PRIVATE_POINTER_INSTANCE     3
#define SINGLETON_INIT_METHOD               STATIC_PRIVATE_POINTER_INSTANCE


/**
 * This Singleton template can be inherited and the parent class can make its constructor
 * private with this class as a friend to inherit this Singleton pattern.
 *
 * @ingroup Utilities
 */

template <typename classType>
class SingletonTemplate
{	
		protected:
				#if (SINGLETON_INIT_METHOD == STATIC_PRIVATE_POINTER_INSTANCE)
						static SingletonTemplate* mpSingletonInstance;
				#endif

				/**
				 * Protected Constructor for this Singleton Class so only the class
				 * inheriting this template class can make this part of itself
				 */
				SingletonTemplate() {}
					
		public:
				/// Public member to get instance of this SINGLETON class.
				static classType& getInstance();

		private:
				/* No Private Members */
};

#if (SINGLETON_INIT_METHOD == STATIC_POINTER_INSTANCE)
			template <typename classType>
			classType& SingletonTemplate<classType>::getInstance()
			{
					static classType* mpSingletonInstance = new classType();
					return *mpSingletonInstance;
			}
#elif (SINGLETON_INIT_METHOD == STATIC_INSTANCE)
			template <typename classType>
			classType& SingletonTemplate<classType>::getInstance()
			{
					static classType singletonInstance;
					return singletonInstance;
			}
#elif (SINGLETON_INIT_METHOD == STATIC_PRIVATE_POINTER_INSTANCE)
			template <typename classType>
			SingletonTemplate<classType>* SingletonTemplate<classType>::mpSingletonInstance = 0;

			template <typename classType>
			classType& SingletonTemplate<classType>::getInstance()
			{
					if(0 == mpSingletonInstance) {
							mpSingletonInstance = new classType();
					}
					return *(classType*)mpSingletonInstance;
			}
#else
			#error SINGLETON_INIT_METHOD contains invalid value
#endif

#endif /*__SINGLETON_TEMPLATE_HPP */
