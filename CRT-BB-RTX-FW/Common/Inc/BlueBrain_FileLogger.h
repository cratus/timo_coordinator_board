/*    
$DESCRIPTION    : Header file for the logger API.   

$Copyright      : CRATUS TECHNOLOGY INC, 2013-16  

$Project        : BlueBrain  

$Author         : chitrang  

$Warnings       : <if any>  
*/



#ifndef FILE_LOGGER_HPP__
#define FILE_LOGGER_HPP__
#ifdef __cplusplus
extern "C" {
#endif
#include <stdint.h>

	
#define FILE_LOGGER_BUFFER_SIZE      (1 * 2048)     ///< Recommend multiples of 512
#define FILE_LOGGER_NUM_BUFFERS      20             ///< Number of buffers (need to have enough while file is being written)
#define FILE_LOGGER_LOG_MSG_MAX_LEN  150            ///< Max length of a log message
#define FILE_LOGGER_FILENAME         "/a/logger.csv"    ///< Destination filename
#define FILE_LOGGER_STACK_SIZE       (3 * 512 / 4)  ///< Stack size in 32-bit (1 = 4 bytes for 32-bit CPU)
#define FILE_LOGGER_FLUSH_TIME_SEC   (1 * 60)       ///< Logs are flushed after this time
#define FILE_LOGGER_BLOCK_TIME_MS    (10)           ///< If no buffer available within this time, block time counter will increment

	
	
typedef enum {
    log_debug,  ///< Debug logs are printed to stdio (printf) unless disabled by logger_set_printf()
    log_info,
    log_warn,
    log_error,
    log_last, ///< Marks the last entry, do not use
} logger_msg_t;
	
	
void logger_init(uint8_t logger_priority);
	
	
void logger_set_printf(logger_msg_t type, bool enable);

#define LOG_ERROR(msg, p...)  logger_log (log_error, __FILE__, __FUNCTION__, __LINE__, msg, ## p)
#define LOG_WARN(msg, p...)   logger_log (log_warn,  __FILE__, __FUNCTION__, __LINE__, msg, ## p)
#define LOG_INFO(msg, p...)   logger_log (log_info,  __FILE__, __FUNCTION__, __LINE__, msg, ## p)
#define LOG_DEBUG(msg, p...)  logger_log (log_debug, __FILE__, __FUNCTION__, __LINE__, msg, ## p)


#define LOG_SIMPLE_MSG(msg, p...)       logger_log (log_info, NULL, NULL, 0, msg, ## p)


#define LOG_RAW_MSG(msg, p...)          logger_log_raw(msg, ## p)

#define LOG_FLUSH()                     logger_send_flush_request()

void logger_send_flush_request(void);

uint32_t logger_get_logged_call_count(logger_msg_t severity);

uint16_t logger_get_blocked_call_count(void);

uint16_t logger_get_highest_file_write_time_ms(void);

uint16_t logger_get_num_buffers_watermark(void);
	
void logger_log(logger_msg_t type, const char * filename, const char * func_name, unsigned line_num,
                const char * msg, ...);

void logger_log_raw(const char * msg, ...);
	
	
	
	
	
	
	
	
	#ifdef __cplusplus
}
#endif
#endif /* FILE_LOGGER_HPP__ */