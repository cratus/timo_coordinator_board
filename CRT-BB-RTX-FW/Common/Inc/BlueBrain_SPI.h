/*
$DESCRIPTION		: This file contains definitions for SPI functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __BLUEBRAIN_HAL_SPI_H
#define __BLUEBRAIN_HAL_SPI_H

	 
/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "singleton_template.h"
#include "BlueBrain_init.h"

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi2;

/** @{ Functions to chip-select, de-select devices and read pin signals */
static inline bool board_io_flash_cs(void)  { GPIOA->BSRR = (1U << 31);   return 1; }
static inline bool board_io_flash_ds(void)  { GPIOA->BSRR = (1U << 15);   return 0; }

class User_SPI : public SingletonTemplate<User_SPI>
{
		public :
					bool init();
					bool transmit(uint8_t* pData);
					bool receive(uint8_t* pData, uint16_t Size);
					bool transfer(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size);
				
		private :
					SPI_HandleTypeDef user_spi_handle;
					User_SPI() { }
					friend class SingletonTemplate<User_SPI>;
};

#endif /*__BLUEBRAIN_HAL_SPI_H */
