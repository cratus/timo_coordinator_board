#ifndef BLUEBRAIN_FILEAPI__
#define BLUEBRAIN_FILEAPI__

#include <stdio.h>
#include "ionfs.h"
#include "ion_lib.h"
#include "ftl_if_ex.h"


class BB_File_API
{
	public:
		static bool f_read(const char *pFilename, void *pData, unsigned int bytesToRead);	
	
		static bool f_create(const char* pFilename, mod_t mode);
	
		static bool f_write(const char *pFilename, void *pData, unsigned int bytesToWrite);		
	
		static bool f_append(const char *pFilename, void *pData, unsigned int bytesToAppend);
	
		static bool f_delete(const char *pFilename);
	
		static void read_whole_file(const char *pFilename);
	
	private:
		BB_File_API() {}

};


#endif 
