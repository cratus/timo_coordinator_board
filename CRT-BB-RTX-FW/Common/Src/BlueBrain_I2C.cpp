/*
$DESCRIPTION		: This file contains definitions for I2C functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_I2C.h"
#include <stdbool.h>

//  The Sensor I2C bus is wired to a precision temperature sensor and a motion processor.
bool
Sensor_I2C :: init(unsigned int speedInKhz)
{
	// Sensor SCL - PB8
	// Sensor SDA - PB7
	// Sensor INT - PC12 - Falling edge interrupt
	sensor_i2c->Instance = I2C1;					
  sensor_i2c->Init.ClockSpeed = speedInKhz * 1000;
  sensor_i2c->Init.DutyCycle = I2C_DUTYCYCLE_2;
  sensor_i2c->Init.OwnAddress1 = 0;
  sensor_i2c->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  sensor_i2c->Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  sensor_i2c->Init.OwnAddress2 = 0;
  sensor_i2c->Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  sensor_i2c->Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
  uint8_t status = HAL_I2C_Init(sensor_i2c);
	
	if(status == HAL_OK) {
		return true;
	}
	else {
			return false;
	}
}

bool 
I2C :: init(unsigned int speedInKhz)
{
	// User SCL - PB10
	// User SDA - PB11
	handle_i2c->Instance = I2C2;
  handle_i2c->Init.ClockSpeed = speedInKhz * 1000;
  handle_i2c->Init.DutyCycle = I2C_DUTYCYCLE_2;
  handle_i2c->Init.OwnAddress1 = 0;
  handle_i2c->Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  handle_i2c->Init.DualAddressMode = I2C_DUALADDRESS_DISABLED;
  handle_i2c->Init.OwnAddress2 = 0;
  handle_i2c->Init.GeneralCallMode = I2C_GENERALCALL_DISABLED;
  handle_i2c->Init.NoStretchMode = I2C_NOSTRETCH_DISABLED;
  uint8_t status = HAL_I2C_Init(handle_i2c);
	
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
I2C ::start()
{
	/* Check the I2C handle allocation */
  if(handle_i2c == NULL) {
    return false;
  }

  /* Check the parameters */
  assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));

  handle_i2c->State = HAL_I2C_STATE_READY;

  /* Disable the I2C Peripheral Clock */
  __HAL_I2C_ENABLE(handle_i2c);
	
	return true;
}

bool
I2C ::stop()
{
	/* Check the I2C handle allocation */
  if(handle_i2c == NULL) {
    return false;
  }

  /* Check the parameters */
  assert_param(IS_I2C_ALL_INSTANCE(hi2c->Instance));

  handle_i2c->State = HAL_I2C_STATE_RESET;

  /* Disable the I2C Peripheral Clock */
  __HAL_I2C_DISABLE(handle_i2c);
	
	return true;
}

bool
I2C :: restart()
{
	stop();
	HAL_Delay(100);
	return start();
}

bool
I2C :: sendbyte(uint16_t DevAddress, uint8_t pData)
{
	uint8_t status = HAL_I2C_Master_Transmit_IT(handle_i2c,DevAddress, &pData, 1);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
I2C :: readbyte(uint16_t DevAddress, uint8_t pData)
{
	uint8_t status = HAL_I2C_Master_Receive_IT(handle_i2c,DevAddress, &pData, 1);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
I2C :: write(uint16_t DevAddress, uint8_t* pData, uint16_t Size)
{
	uint8_t status = HAL_I2C_Master_Transmit(handle_i2c,DevAddress, pData, Size,200);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
I2C :: read(uint16_t DevAddress, uint8_t* pData, uint16_t Size)
{
	uint8_t status = HAL_I2C_Master_Receive_IT(handle_i2c,DevAddress, pData, Size);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}
