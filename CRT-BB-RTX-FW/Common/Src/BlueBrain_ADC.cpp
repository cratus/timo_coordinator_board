/*
$DESCRIPTION		: This file contains definitions for ADC functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include <stdbool.h>
#include "BlueBrain_ADC.h"

ADC_HandleTypeDef hadc1[ADC_HADC1_LIMIT];

//ADC_config - Configure the ADC channel number with specified resolution
void 
ADC_config(uint8_t channel_no, uint32_t bitcount)
{
	ADC_ChannelConfTypeDef sConfig;

  // Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
    
	if (channel_no < ADC_CHANNEL_NO_3) {
		hadc1[channel_no].Instance = ADC1;
	}
	else {
		hadc1[channel_no].Instance = ADC2;
	}
  hadc1[channel_no].Init.ClockPrescaler = ADC_CLOCKPRESCALER_PCLK_DIV4;
  hadc1[channel_no].Init.Resolution = bitcount;
  hadc1[channel_no].Init.ScanConvMode = DISABLE;
  hadc1[channel_no].Init.ContinuousConvMode = DISABLE;
  hadc1[channel_no].Init.DiscontinuousConvMode = DISABLE;
  hadc1[channel_no].Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc1[channel_no].Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc1[channel_no].Init.NbrOfConversion = 1;
  hadc1[channel_no].Init.DMAContinuousRequests = DISABLE;
  hadc1[channel_no].Init.EOCSelection = EOC_SINGLE_CONV;
  HAL_ADC_Init(&hadc1[channel_no]);

  // Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
    
	switch(channel_no) {
		case 0:
			sConfig.Channel = ADC_CHANNEL_6;
			break;
		case 1:
			sConfig.Channel = ADC_CHANNEL_7;
			break;
		case 2:
			sConfig.Channel = ADC_CHANNEL_TEMPSENSOR;
			break;
		case 3:
			sConfig.Channel = ADC_CHANNEL_4;
			break;
		case 4:
			sConfig.Channel = ADC_CHANNEL_5;
			break;
	}
  
	sConfig.Rank = 1;
  sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
  HAL_ADC_ConfigChannel(&hadc1[channel_no], &sConfig);
}


//ADC_read - Read the ADC data for the specified channel number
uint32_t 
ADC_read(uint8_t channel_no)
{
	HAL_ADC_Start(hadc1);
	HAL_ADC_PollForConversion(&hadc1[channel_no],1);
	HAL_ADC_Stop(hadc1);
	return HAL_ADC_GetValue(&hadc1[channel_no]);
}
