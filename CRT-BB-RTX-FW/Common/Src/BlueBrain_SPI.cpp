/*
$DESCRIPTION		: This file contains definitions for SPI functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include "BlueBrain_SPI.h"
#include <stdbool.h>
#include <string.h>


/**
  * @brief  Initialize User SPI bus in Master mode with default conditions
  * @retval if initialize properly -> true, otherwise false
  */	
bool
User_SPI :: init()
{
	hspi1.Instance = SPI1;																		// SPI1 -> SCK-PB3, MOSI-PB5, MISO-PB4, CS-PA15 
  hspi1.Init.Mode = SPI_MODE_MASTER;												// Master mode
  hspi1.Init.Direction = SPI_DIRECTION_2LINES;							// SPI direction mode -> two line
  hspi1.Init.DataSize = SPI_DATASIZE_8BIT;									// SPI data size -> 8 bits
  hspi1.Init.CLKPolarity = SPI_POLARITY_LOW;								// SPI polarity -> Low
  hspi1.Init.CLKPhase = SPI_PHASE_1EDGE;
  hspi1.Init.NSS = SPI_NSS_HARD_OUTPUT;
  hspi1.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  hspi1.Init.FirstBit = SPI_FIRSTBIT_MSB;										// SPI transmitting first bit is MSB
  hspi1.Init.TIMode = SPI_TIMODE_DISABLED;
  hspi1.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLED;
  hspi1.Init.CRCPolynomial = 10;
  uint8_t status = HAL_SPI_Init(&hspi1);										// Initialize HAL_SPI 
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
User_SPI :: transmit(uint8_t* pData)
{
	uint8_t status = HAL_SPI_Transmit(&hspi1, pData, 1,100);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
User_SPI :: receive(uint8_t* pData, uint16_t Size)
{
	uint8_t status = HAL_SPI_Receive_IT(&user_spi_handle, pData, Size);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}

bool
User_SPI :: transfer(uint8_t *pTxData, uint8_t *pRxData, uint16_t Size)
{
	uint8_t status = HAL_SPI_TransmitReceive_IT(&user_spi_handle, pTxData, pRxData, Size);
	if(status == HAL_OK) {
		return true;
	}
	else {
		return false;
	}
}
