/*
$DESCRIPTION		: This file contains definitions for UART functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Dhruv

*/

#include <stdbool.h>
#include "BlueBrain_UART.h"
#include "BlueBrain_io.h"

extern void buffer_Init (void);
extern void RS_485_buffer_Init (void);
HAL_StatusTypeDef 
User_UART :: init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits)
{
		user_uart.Instance = USART2;								//		TX - PA2			RX - PA3
		user_uart.Init.BaudRate = baudrate;
		user_uart.Init.WordLength = databits;
		user_uart.Init.StopBits = stopbits;
		user_uart.Init.Parity = parity;
		user_uart.Init.Mode = UART_MODE_TX_RX;
		user_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
		user_uart.Init.OverSampling = UART_OVERSAMPLING_16;
		return HAL_UART_Init(&user_uart);
}

bool 
User_UART :: print_EOL(uint8_t EOL_setting)
{
		print_type = EOL_setting;
		return true;
}

void 
User_UART :: print_echo(bool on)
{
		echo = on;
}

HAL_StatusTypeDef 
User_UART :: print(const char *pData)
{
		if (!pData) {
				return HAL_OK;
		}
		while(*pData) {
						HAL_UART_Transmit(&(user_uart),(uint8_t*)pData++,1,100);
		}
		return HAL_OK;
}

HAL_StatusTypeDef 
User_UART :: print(unsigned int lcd_num)
{
	int tlcd_num;
	short len;
	char *lcd_string,num[11];
	lcd_string = num;
	uint8_t hel = 48;
	//--------------------------------
	if(lcd_num == 0)
		HAL_UART_Transmit(&(user_uart),&hel,1,100);
	//--------------------------------
	tlcd_num = lcd_num;
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = lcd_num%10 + 48;
		lcd_num /= 10;	
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef 
User_UART :: print(int32_t lcd_num)
{
	int tlcd_num;
	short len,sign = 0;
	char *lcd_string,num[11];
	lcd_string = num;
	uint8_t hel = 48;
	//--------------------------------
	if(lcd_num == 0) {
		HAL_UART_Transmit(&(user_uart),&hel,1,100);
	}
	else if(lcd_num < 0) {
		sign = 1;
		*lcd_string++ = '-';
		lcd_num = -lcd_num;
	}
	//--------------------------------
	tlcd_num = lcd_num;							  
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = (lcd_num%10) + 48;
		lcd_num /= 10;
	}
	if(sign == 1) {
		lcd_string--;
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef 
User_UART :: println(const char *pData)
{
		print(pData);
		switch(print_type)
		{
			case UART_EOL_NULL:
				HAL_UART_Transmit(&(user_uart),(uint8_t*)"\0",1,100);
				break;
			case UART_EOL_CR:
				HAL_UART_Transmit(&(user_uart),(uint8_t*)"\r",1,100);
				break;
			case UART_EOL_LF:
				HAL_UART_Transmit(&(user_uart),(uint8_t*)"\n",1,100);
				break;
			case UART_EOL_CRLF:
				HAL_UART_Transmit(&(user_uart),(uint8_t*)"\r\n",2,100);
				break;
			default:
					break;
		}
		return HAL_OK;
}

HAL_StatusTypeDef 
Debug_UART :: init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits)
{
		
		debug_uart.Instance = UART4;										// TX - PC10		RX - PC11
		debug_uart.Init.BaudRate = baudrate;
		debug_uart.Init.WordLength = databits;
		debug_uart.Init.StopBits = stopbits;
		debug_uart.Init.Parity = parity;
		debug_uart.Init.Mode = UART_MODE_TX_RX;
		debug_uart.Init.HwFlowCtl = UART_HWCONTROL_NONE;
		debug_uart.Init.OverSampling = UART_OVERSAMPLING_16;
		HAL_UART_Init(&debug_uart);
	#ifdef TERMINAL_ENABLE
		buffer_Init(); 
	#endif
		RS_485_buffer_Init();  // -TIMO- project
		return HAL_OK;
}

UART_HandleTypeDef* 
Debug_UART :: get_uart(void)
{
	return &debug_uart;
}

bool 
Debug_UART :: raw_print(uint8_t *pData)
{
		if (!pData) {
				return HAL_OK;
		}
		while(*pData) {
						HAL_UART_Transmit(&(debug_uart),(uint8_t*)pData++,1,100);
		}
		return HAL_OK;
}

bool 
Debug_UART :: new_print(uint8_t pData)
{

		HAL_UART_Transmit(&(debug_uart),&pData,1,100);
		return HAL_OK;
}

HAL_StatusTypeDef 
Debug_UART :: print(const char *pData)
{
		if (!pData) {
				return HAL_OK;
		}
		while(*pData) {
						HAL_UART_Transmit(&(debug_uart),(uint8_t*)pData++,1,100);
		}
		return HAL_OK;
}


HAL_StatusTypeDef 
Debug_UART :: print(uint32_t lcd_num)
{
	int tlcd_num;
	short len;
	char *lcd_string,num[11];
	lcd_string = num;
	uint8_t hel = 48;
	//--------------------------------
	if(lcd_num == 0) {
		HAL_UART_Transmit(&(debug_uart),&hel,1,100);
	}
	//--------------------------------
	tlcd_num = lcd_num;
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = lcd_num%10 + 48;
		lcd_num /= 10;	
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef 
Debug_UART :: print(int32_t lcd_num)
{
	int tlcd_num;
	short len,sign = 0;
	char *lcd_string,num[32];
	lcd_string = num;
	uint8_t hel = 48;
	//--------------------------------
	if(lcd_num == 0) {
		HAL_UART_Transmit(&(debug_uart),&hel,1,100);
	}
	else if(lcd_num < 0)	{
		sign = 1;
		*lcd_string++ = '-';
		lcd_num = -lcd_num;
	}
	//--------------------------------
	tlcd_num = lcd_num;							  
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = (lcd_num%10) + 48;
		lcd_num /= 10;
	}
	if(sign == 1) {
		lcd_string--;
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef 
Debug_UART :: println(const char *pData)
{
		print(pData);
		switch(print_type)
		{
			case UART_EOL_NULL:
				HAL_UART_Transmit(&(debug_uart),(uint8_t*)"\0",1,100);
				break;
			case UART_EOL_CR:
				HAL_UART_Transmit(&(debug_uart),(uint8_t*)"\r",1,100);
				break;
			case UART_EOL_LF:
				HAL_UART_Transmit(&(debug_uart),(uint8_t*)"\n",1,100);
				break;
			case UART_EOL_CRLF:
				HAL_UART_Transmit(&(debug_uart),(uint8_t*)"\r\n",2,100);
				break;
			default:
					break;
		}
		return HAL_OK;
}

HAL_StatusTypeDef 
BLE_UART :: init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits)
{
		ble_uart.Instance = USART1;														// TX - PA9			RX - PA10			CTS - PA11			RTS - PA12
		ble_uart.Init.BaudRate = baudrate;
		ble_uart.Init.WordLength = databits;
		ble_uart.Init.StopBits = stopbits;
		ble_uart.Init.Parity = parity;
		ble_uart.Init.Mode = UART_MODE_TX_RX;
		ble_uart.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
		ble_uart.Init.OverSampling = UART_OVERSAMPLING_16;
		return HAL_UART_Init(&ble_uart);
}

UART_HandleTypeDef* 
BLE_UART :: get_uart(void)
{
	return &ble_uart;
}

HAL_StatusTypeDef 
BLE_UART :: print(const char *pData)
{
		if (!pData) {
				return HAL_OK;
		}
		while(*pData) {
						HAL_UART_Transmit(&(ble_uart),(uint8_t*)pData++,1,100);
		}
		return HAL_OK;
}

HAL_StatusTypeDef 
BLE_UART :: print(unsigned int lcd_num)
{
	int tlcd_num;
	short len;
	char *lcd_string,num[11];
	lcd_string = num;
	//--------------------------------
	if(lcd_num == 0) {
		HAL_UART_Transmit(&(ble_uart),(uint8_t*)"0",1,100);
	}
	//--------------------------------
	tlcd_num = lcd_num;
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = lcd_num%10 + 48;
		lcd_num /= 10;	
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef
BLE_UART :: print(signed int lcd_num)
{
	int tlcd_num;
	short len,sign = 0;
	char *lcd_string,num[11];
	lcd_string = num;
	//--------------------------------
	if(lcd_num == 0) {
		print('0');		
	}
	else if(lcd_num < 0)	{
		sign = 1;
		*lcd_string++ = '-';
		lcd_num = -lcd_num;
	}
	//--------------------------------
	tlcd_num = lcd_num;							  
	for(len=0;tlcd_num>0;len++,tlcd_num/=10);
	lcd_string += len;
	*lcd_string-- = 0;
	for(;len>0;len--)	{
		*lcd_string-- = (lcd_num%10) + 48;
		lcd_num /= 10;
	}
	if(sign == 1) {
		lcd_string--;
	}
	lcd_string++;
	//--------------------------------
	print(lcd_string);
	return HAL_OK;
}

HAL_StatusTypeDef 
BLE_UART :: println(uint8_t *pData)
{
	uint16_t Size = sizeof(pData);
	switch(print_type)
	{
		case UART_EOL_NULL:
			pData[Size] = '\0';
			break;
		case UART_EOL_CR:
			pData[Size++] = '\r';
			break;
		case UART_EOL_LF:
			pData[Size++] = '\n';
			break;
		case UART_EOL_CRLF:
			pData[Size++] = '\r';
			pData[Size++] = '\n';
			break;
		default:
				break;
	}
	/*
	 *  Sends an amount of data in non blocking mode.
	*/
	return HAL_UART_Transmit(&(ble_uart),pData,Size,100);
}

bool 
BLE_UART :: send_data(uint8_t *pData, uint8_t Size)
{
	uint8_t status = HAL_UART_Transmit(&(huart1),pData,Size,1000);
	if(status == HAL_OK) {
			return true;
	}
	else{
			return false;
	}
}

HAL_StatusTypeDef 
BLE_UART :: read(uint8_t *pData, uint16_t Size)
{
	return HAL_UART_Receive_IT(&(ble_uart),pData,Size);
}

bool 
BLE_UART :: readln(uint8_t *pData, uint16_t Size)
{
	uint16_t max_size = 65535;
	uint16_t i = 0;
	uint8_t* tmp = pData;
	bool status = false;
	while(Size<max_size) {
		HAL_UART_Receive_IT(&(ble_uart),pData++,1);
		if(pData[i]==print_type) {
			status = true;
			break;
		}
	}
	pData = tmp;
	return status;
}

bool 
BLE_UART :: Kick_BLE()
{
	HAL_GPIO_WritePin(GPIOD, KICK_BLE_RESET_Pin, GPIO_PIN_RESET);
	HAL_Delay(100);
	HAL_GPIO_WritePin(GPIOD, KICK_BLE_RESET_Pin, GPIO_PIN_SET);
	return true;
}

HAL_StatusTypeDef 
Debug_UART :: read(uint8_t *pData, uint16_t Size)
{
	return HAL_UART_Receive_IT(&(debug_uart),pData,Size);
}

bool 
Debug_UART :: readln(uint8_t *pData, uint16_t Size)
{
	uint16_t max_size = 65535;
	uint16_t i = 0;
	uint8_t* tmp = pData;
	bool status = false;
	while(Size<max_size) {
		HAL_UART_Receive_IT(&debug_uart,pData++,1);
		if(pData[i]==print_type) {
			status = true;
			break;
		}
	}
	pData = tmp;
	return status;
}

HAL_StatusTypeDef 
User_UART :: read(uint8_t *pData, uint16_t Size)
{
	return HAL_UART_Receive_IT(&user_uart,pData,Size);
}

bool 
User_UART :: readln(uint8_t *pData, uint16_t Size)
{
	uint16_t max_size = 65535;
	uint16_t i = 0;
	uint8_t* tmp = pData;
	bool status = false;
	while(Size<max_size) {
		HAL_UART_Receive_IT(&user_uart,pData++,1);
		if(pData[i]==print_type) {
			status = true;
			break;
		}
	}
	pData = tmp;
	return status;
}

/* Porting Printf function to different UART Ports */

HAL_StatusTypeDef HAL_Status;

extern "C"
{
	#if DEBUG_CONSOLE
		int 
		fputc(int ch, FILE *f)  // Retarget printf() to USART2
		{
			HAL_Status = HAL_UART_Transmit(DEBUG_UART.get_uart(), (uint8_t*) &ch, 1, 2000);

			return ch;
		}

		int 
		fgetc(FILE *f)  // Retarget scanf() to USART2
		{
			uint8_t TempByte;

			HAL_Status = HAL_UART_Receive(DEBUG_UART.get_uart(), (uint8_t*) &TempByte, 1, 2000);

			return TempByte;
		}
	#elif USER_CONSOLE
		int 
		fputc(int ch, FILE *f)  // Retarget printf() to USART2
		{
			HAL_Status = HAL_UART_Transmit(USER_UART.get_uart(), (uint8_t*) &ch, 1, 2000);

			return ch;
		}

		int 
		fgetc(FILE *f)  // Retarget scanf() to USART2
		{
			uint8_t TempByte;

			HAL_Status = HAL_UART_Receive(USER_UART.get_uart(), (uint8_t*) &TempByte, 1, 2000);

			return TempByte;
		}
	#elif BLE_CONSOLE
		int 
		fputc(int ch, FILE *f)  // Retarget printf() to USART2
		{
			HAL_Status = HAL_UART_Transmit(BLE.get_uart(), (uint8_t*) &ch, 1, 2000);

			return ch;
		}

		int 
		fgetc(FILE *f)  // Retarget scanf() to USART2
		{
			uint8_t TempByte;

			HAL_Status = HAL_UART_Receive(BLE.get_uart(), (uint8_t*) &TempByte, 1, 2000);

			return TempByte;
		}
	#endif
}

