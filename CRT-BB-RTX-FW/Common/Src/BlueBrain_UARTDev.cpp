/*
$DESCRIPTION		: This file contains definitions for UART functions of BlueBrain

$Copyright			: CRATUS TECHNOLOGY INC, 2013-16 

$Project				: BLUEBRAIN

$Author					: Chitrang

*/


#include "BlueBrain_UARTDev.h"
#include <stdbool.h>


/* Protected Constructor */

UartDev::UartDev(unsigned int* pUARTBaseAddr) : mUARTRegBase((UART_HandleTypeDef*) pUARTBaseAddr)
{
	
}


HAL_StatusTypeDef UartDev :: init(uint32_t baudrate, uint32_t parity, uint32_t databits, uint32_t stopbits)
{
		mUARTRegBase->Instance = USART2;								//		TX - PA2			RX - PA3
		mUARTRegBase->Init.BaudRate = baudrate;
		mUARTRegBase->Init.WordLength = databits;
		mUARTRegBase->Init.StopBits = stopbits;
		mUARTRegBase->Init.Parity = parity;
		mUARTRegBase->Init.Mode = UART_MODE_TX_RX;
		mUARTRegBase->Init.HwFlowCtl = UART_HWCONTROL_NONE;
		mUARTRegBase->Init.OverSampling = UART_OVERSAMPLING_16;
		return HAL_UART_Init(mUARTRegBase);
}