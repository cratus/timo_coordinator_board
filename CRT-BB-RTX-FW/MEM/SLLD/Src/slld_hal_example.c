/* slld_hal_example.c - SLLD Hardware Abstraction Layer example Code */
 
/**************************************************************************
* Copyright 2011 Spansion LLC. All Rights Reserved. 
*
* This software is owned and published by: 
* Spansion LLC, 915 DeGuigne Drive, Sunnyvale, CA 94088 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes driver source code for use in programming Spansion's 
* Flash memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* Spansion MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  Spansion WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF Spansion HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/

#include <stdio.h>
#include "slld_targetspecific.h"
#include "slld.h"
#include "slld_hal.h"
#include "stm32f4xx_hal.h"
extern SPI_HandleTypeDef hspi2;
extern bool status_getFlag;
/**
 * Exchanges a byte over SPI bus
 * @param out   The byte to send out
 * @returns     The byte received over SPI
 */
static inline char ssp1_exchange_byte(uint8_t out)
{
	  uint8_t rx_byte=0;
    HAL_SPI_TransmitReceive(&hspi2,&out,&rx_byte, 1, 100);
		return rx_byte;
}

/**
 * Writes a byte to the SPI FIFO
 * @warning YOU MUST ENSURE SPI IS NOT BUSY before calling this function
 */
static inline char ssp1_exchange_data(uint8_t data, int len)
{
		uint8_t received_byte=0;
    HAL_SPI_TransmitReceive(&hspi2, &data,&received_byte, len, 100);
		return received_byte;
}

// ***************************************************************************
//  FLASH_READ - HAL read function
//
//  input : device_num            device number to which operation will be done
//          command               write a single command byte to flash
//          sys_addr              system address to be used
//          data_buffer           Pointer to the data buffer where to store the read data
//          Number_Of_Read_Bytes  number of bytes to be read
//
//  return value : status of the operation - FAIL or SUCCESS
// ***************************************************************************
SLLD_STATUS FLASH_READ
(
BYTE     command,                        /* write a single command byte to flash */
ADDRESS  sys_addr,                       /* system address to be used */
BYTE    *data_buffer,                    /* Pointer to the data buffer containing data to be written */
int      Number_Of_Read_Bytes            /* number of bytes to be read */
)
{
    SLLD_STATUS status = SLLD_OK;
    int data_cycle, Number_Of_Dummy_Bytes = 0;
		uint8_t dummy_byte = 0;
    
    
    // Write the command
    ssp1_exchange_data(command,1);

    // Write the address
    if (sys_addr != ADDRESS_NOT_USED)
    {
        switch (command)
        {
            case SPI_DUALIO_HPRD_CMD:
            case SPI_DUALIO_HPRD_4B_CMD:
            {
                ssp1_exchange_data((uint8_t)((sys_addr >> 16) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t)((sys_addr >>  8) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t) (sys_addr        & 0x000000FF),1);
                break;
            }
            case SPI_QUADIO_HPRD_CMD:
            case SPI_QUADIO_HPRD_4B_CMD:
            {
                ssp1_exchange_data((uint8_t)((sys_addr >> 16) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t)((sys_addr >>  8) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t)(sys_addr        & 0x000000FF),1);
                break;
            }
            default:
            {
                ssp1_exchange_data((uint8_t)((sys_addr >> 16) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t)((sys_addr >>  8) & 0x000000FF),1);
                ssp1_exchange_data((uint8_t) (sys_addr        & 0x000000FF),1);
								
                break;
            }
        }
    }

    // Write the dummy bytes
    switch (command)
    {
        case SPI_FAST_READ_CMD:
        case SPI_FAST_READ_4B_CMD:
        case SPI_DUALIO_RD_CMD:
        case SPI_DUALIO_RD_4B_CMD:
        case SPI_QUADIO_RD_CMD:
        case SPI_QUADIO_RD_4B_CMD:
        case SPI_OTPR_CMD:
        {
            Number_Of_Dummy_Bytes = 1;
            // Write a dummy byte to the data bus
            for (data_cycle = 0; data_cycle < Number_Of_Dummy_Bytes; data_cycle++)
                ssp1_exchange_data(dummy_byte,1);
            break;
        }
        case SPI_DUALIO_HPRD_CMD:
        case SPI_DUALIO_HPRD_4B_CMD:
        {
            Number_Of_Dummy_Bytes = 1;
            // Write a dummy byte to the data bus - This is actually the mode bit
            for (data_cycle = 0; data_cycle < Number_Of_Dummy_Bytes; data_cycle++)
                ssp1_exchange_data(dummy_byte,1);
            break;
        }
        case SPI_QUADIO_HPRD_CMD:
        case SPI_QUADIO_HPRD_4B_CMD:
        {
            Number_Of_Dummy_Bytes = 3;
            // Write the dummy bytes to the data bus - The first byte is actually the mode bit
            for (data_cycle = 0; data_cycle < Number_Of_Dummy_Bytes; data_cycle++)
                ssp1_exchange_data(dummy_byte,1);
            break;
        }
        case SPI_RES_CMD:
        {
						
            Number_Of_Dummy_Bytes = 3;
            // Write the dummy bytes to the data bus
            for (data_cycle = 0; data_cycle < Number_Of_Dummy_Bytes; data_cycle++)
                ssp1_exchange_data(dummy_byte,1);
            break;
        }
        default:
        {
            Number_Of_Dummy_Bytes = 0;
            break;
        }
    }

    // Read the data
    if (Number_Of_Read_Bytes != 0)
    {
        switch (command)
        {
            case SPI_DUALIO_RD_CMD:
            case SPI_DUALIO_RD_4B_CMD:
            case SPI_DUALIO_HPRD_CMD:
            case SPI_DUALIO_HPRD_4B_CMD:
            {
                // Read the data using the relevant mode
                for (data_cycle = 0; data_cycle < Number_Of_Read_Bytes; data_cycle++)
                    *(data_buffer + data_cycle) = ssp1_exchange_byte(0);
                break;
            }
            case SPI_QUADIO_RD_CMD:
            case SPI_QUADIO_RD_4B_CMD:
            case SPI_QUADIO_HPRD_CMD:
            case SPI_QUADIO_HPRD_4B_CMD:
            {
                // Read the data using the relevant mode
                for (data_cycle = 0; data_cycle < Number_Of_Read_Bytes; data_cycle++)
                    *(data_buffer + data_cycle) = ssp1_exchange_byte(0);
                break;
            }
            default:
            {
                // Read the data using the relevant mode
								
                for (data_cycle = 0; data_cycle < Number_Of_Read_Bytes; data_cycle++)
								{
									*(data_buffer + data_cycle) = ssp1_exchange_data(0, 1);
										//DEBUG_UART.print(*(data_buffer + data_cycle));
										//DEBUG_UART.print(" ");
								}			
                break;
            }
        }
    }

    if(!status_getFlag)
			__HAL_SPI_DISABLE(&hspi2);
    return(status);
}


// ***************************************************************************
//  FLASH_WRITE - HAL write function
//
//  input : device_num               device number to which operation will be done
//          command                  write a single command byte to flash
//          sys_addr                 system address to be used
//          data_buffer              Pointer to the data buffer where to store the written data
//          Number_Of_Written_Bytes  number of bytes to be written
//
//  return value : status of the operation - FAIL or SUCCESS
// ***************************************************************************
SLLD_STATUS FLASH_WRITE
(
BYTE     command,                        /* write a single command byte to flash */
ADDRESS  sys_addr,                       /* system address to be used */
BYTE    *data_buffer,                    /* Pointer to the data buffer containing data to be written */
int      Number_Of_Written_Bytes         /* number of bytes to be written */
)
{
    SLLD_STATUS status = SLLD_OK;
		int data_cycle;
		uint8_t rx_byte;

    // Write the command
    ssp1_exchange_data(command,1);

    // Write the address
    if (sys_addr != ADDRESS_NOT_USED)
    {
					
					ssp1_exchange_data((uint8_t)((sys_addr >> 16) & 0x000000FF),1);
          ssp1_exchange_data((uint8_t)((sys_addr >>  8) & 0x000000FF),1);
          ssp1_exchange_data((uint8_t) (sys_addr        & 0x000000FF),1);
    }

    // Write the data
    if (Number_Of_Written_Bytes != 0)
    {
        switch (command)
        {
            case SPI_QPP_CMD:
            case SPI_QPP_4B_CMD:
            {
                // Write the data using the relevant mode
                for (data_cycle = 0; data_cycle < Number_Of_Written_Bytes; data_cycle++)
                    ssp1_exchange_byte(*(data_buffer + data_cycle));
                break;
            }
            default:
            {
								
                // Write the data using the relevant mode
                for (data_cycle = 0; data_cycle < Number_Of_Written_Bytes; data_cycle++)
							{		
                     ssp1_exchange_data(*(data_buffer + data_cycle),1);
									//	HAL_SPI_TransmitReceive(&hspi2,(data_buffer + data_cycle),&rx_byte, 1, 100);
									//	DEBUG_UART.print(*(data_buffer + data_cycle));
									//	DEBUG_UART.println(" ");
							}
                break;
            }
        }
    }

		__HAL_SPI_DISABLE(&hspi2);

    return(status);
}


/*****************************************************************************/
