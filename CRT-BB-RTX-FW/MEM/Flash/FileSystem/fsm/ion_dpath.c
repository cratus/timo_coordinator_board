/* FILE: ion_dpath.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "ion_dpath.h"



/*-----------------------------------------------------------------------------
 DEFINE GLOVAL VARIABLES & DEFINITIONS
-----------------------------------------------------------------------------*/

#if defined( IONFS_FORBID_CHAR )
/* The file name forbids the following characters. */
char_t forbid_chars[] = { ':', '*', '?', '\"', '<', '>', '|' };
/* Check the forbidden characters. */
#define IS_FORBID_CHAR(ch)  (((ch)==':') || ((ch)=='*') || ((ch)=='?') || \
                            ((ch)=='\"') || ((ch)=='<') || ((ch)=='>') || \
                            ((ch)=='|') )
#endif
/* Check the characters for path name. */
#define IS_PATH_CHAR(ch)  (((ch)=='\\') || ((ch)=='/'))




/*-----------------------------------------------------------------------------
 DEFINE FUNCTIONS
-----------------------------------------------------------------------------*/

/*
 Name: fsm_parse_path
 Desc: Parsing the string by a delimiter character('\\' or '/') in given path,
       then put each parsed string to fsm_arg_t structure.
 Params:
   - buf: The string buffer will be filled with NULL character instead of one
          of delimeter in Path.
   - arg: Pointer to the data structure in which the elements parsed are placed.
   - path: Pointer to the buffer to be parsed.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t fsm_parse_path( char_t *buf, fsm_arg_t *arg, const char_t *path )
{
   const char_t *src;
   char_t *dst;
   int32_t argc, i;


   src = path;
   dst = buf;
   argc = i = 0;

   while ( 1 ) {
      while ( IS_PATH_CHAR(*src) )
         src++;

      if ( '\0' == *src ) break;

      dst += i;
      i = 0;
      arg->argv[argc++] = dst;

      if ( IONFS_ALLPATH_ELEMENTS_MAX < argc )
         return os_set_errno( IONFS_ENAMETOOLONG );

      while ( 1 ) {
         if ( '\0' == *src ) {
            dst[i] = '\0';

            /* Ignore the case which finishes with dot or space character. */
            while ( i && ((SPACE_CHAR == dst[i-1]) || (DOT_CHAR == dst[i-1])) )
               dst[--i] = '\0';
            if ( 0 == i ) return os_set_errno( IONFS_EPATH );
            goto End;
         }

         if ( IS_PATH_CHAR(*src) )
            break;

#if defined( IONFS_FORBID_CHAR )
         if ( (SPACE_CHAR > *src) || IS_FORBID_CHAR(*src) )
            return os_set_errno( IONFS_EPATH );
#else
         if ( (SPACE_CHAR > *src) )
            return os_set_errno( IONFS_EPATH );
#endif

         dst[i++] = *src++;

         if ( (IONFS_ALLPATH_LEN_MAX - 2/*volume length*/) < ((int32_t)src - (int32_t)path) )
            return os_set_errno( IONFS_ENAMETOOLONG );
      }


      /* Ignore the case which finishes with dot or space character. */
      while ( i && ((SPACE_CHAR == dst[i-1]) || (DOT_CHAR == dst[i-1])) )
         dst[--i] = '\0';

	  dst[i++] = '\0'; 

      if ( 0 == i ) return os_set_errno( IONFS_EPATH );
   }

End:
   arg->argc = argc;

   return IONFS_OK;
}

/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/

