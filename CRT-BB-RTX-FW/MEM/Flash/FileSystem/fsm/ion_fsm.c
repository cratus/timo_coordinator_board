/* FILE: ion_fsm.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "ion_fsm.h"
#include "../fat/ion_mbr.h"
#include "../fat/ion_fat.h"
#include "../fat/ion_vol.h"
#include "../lim/ion_lim.h"
#include "../osd/ion_osd.h"
#include <stdarg.h>
#include "BlueBrain_IO.h"




/*-----------------------------------------------------------------------------
 DEFINE STRUCTURES
-----------------------------------------------------------------------------*/

/* Type definition to represent each operation at the file system */
typedef int32_t (*fsm_setupcallback_t)(fsm_op_t *);


/* Structure to describe the file system */
typedef struct {
   uint32_t fs_id,  /* A id of file system */
            flag;   /* A falg value of file system */
   fsm_op_t op;     /* File system operations */

} fsm_mng_t;


/* Structure for information of a volume */
typedef struct {
   int32_t vol_id;  /* A volume's id */
   uint32_t flag;   /* A flag value of volume */
   fsm_mng_t *fs;   /* File system manager of volume */

} fsm_volinfo_t;


/* Structure to describe a volume */
typedef struct {
   int16_t vol_id,  /* A volume's id */
           dev_id,  /* A id of device including the volume */
           part_no; /* The partition number of volume */

} fsm_vol_map_t;




/*-----------------------------------------------------------------------------
 DEFINE GLOBAL VARIABLES
-----------------------------------------------------------------------------*/

/* File system mapping table */
#define FSM_ID_FAT 0
#define FSM_ID_NTFS 1

/* File system mapping table */
static fsm_setupcallback_t fsm_setup_callback[IONFS_FILESYSTEM_MAX] = {
   /* 0 : FAT16/32 file system */
   fat_setup,
   /* 1 : NTFS file system */
   /* ntfs_setup, */

   /* ... etc file system */
};




/* The Volume's map table */
static fsm_vol_map_t fsm_vol_map[IONFS_VOLUME_NUM];
/* The count of used volumes */
static uint32_t fsm_next_vol_id;




/* Index of file system manager & volume manager */
#define GET_FSM(id) (&fsm_mng[id])
#define GET_FS_VOL(vol) (&fsm_vol[vol])




/* The maximum length of volume name */
#define IONFS_VOL_NAME_LEN 2




/* File system manager & volume infomation manager */
static fsm_mng_t fsm_mng[IONFS_FILESYSTEM_MAX];
static fsm_volinfo_t fsm_vol[IONFS_VOLUME_NUM];
static bool_t fsm_inited;




/*-----------------------------------------------------------------------------
 DEFINE FUNCTIONS
-----------------------------------------------------------------------------*/

/*
 Name: __get_vol_idx
 Desc: Get the volume's index from the given string.
 Params:
   - vol: The name of volume to get the volume's index.
 Returns:
   int32_t  >= 0 on success. The returned value means the volume's index.
            <  0 on fail.
 Caveats: The value returned is used for the index in the volume's map table.
*/

static int32_t __get_vol_idx( const char_t *vol )
{
   #define fsm_is_path_delimiter(ch) (((ch==(char_t)'\\')||(ch==(char_t)'/')) ? true : false)
   #define FIRST_ID_CHAR ((char_t)'A')
   #define LAST_ID_CHAR ((char_t)'Z')

   char_t ch_vol_id;
   int32_t vol_idx;


   if ( (NULL==vol) || !fsm_is_path_delimiter(vol[0]) || !fsm_is_path_delimiter(vol[2]) )
      return os_set_errno( IONFS_EINVAL );

   ch_vol_id = (char_t)ionFS_t_toupper(vol[1]);

   vol_idx = (int32_t)(ch_vol_id - FIRST_ID_CHAR);

   if ( (0 > vol_idx) || (IONFS_VOLUME_NUM <= vol_idx) )
      return os_set_errno( IONFS_EINVAL );

   return vol_idx;
}




/*
 Name: __get_vol_id
 Desc: Get the volume's id from the given string.
 Params:
   - vol: The name of volume.
 Returns:
   int32_t >=0 on success. The value returned means the volume's id.
           < 0 on fail.
 Caveats: The volume's id is derived from the volume's map table.
*/

static int32_t __get_vol_id( const char_t *vol )
{
   int32_t vol_idx;


   vol_idx = __get_vol_idx( vol );
   if ( 0 > vol_idx ) return vol_idx;

   return fsm_vol_map[vol_idx].vol_id;
}




/*
 Name: __set_vol_id
 Desc: Fill volume's information in the volume's map table.
 Params:
   - vol: The name of volume to set.
   - dev_id: The ID of device to set.
   - part_no: The partition number to set.
 Returns:
   int32_t >=0 on success. The value returned means the volume's id.
           < 0 on fail.
 Caveats: None.
*/

static int32_t __set_vol_id( const char_t *vol, int32_t dev_id, int32_t part_no )
{
   fsm_vol_map_t *vol_map;
   int32_t vol_idx;


   vol_idx = __get_vol_idx( vol );
   if ( 0 > vol_idx )
      return vol_idx;

   vol_map = &fsm_vol_map[vol_idx];

   /* Fill volume's information in map table */
   if ( 0 > vol_map->vol_id ) {
      vol_map->vol_id = (int16_t)(fsm_next_vol_id++);
      vol_map->dev_id = (int16_t)dev_id;
      vol_map->part_no = (int16_t)part_no;
   }
   else if ( vol_map->dev_id != dev_id || vol_map->part_no != part_no )
      return os_set_errno( IONFS_EBUSY );

   return vol_map->vol_id;
}




/*
 Name: __is_valid_vol
 Desc: Check whether the volume of given string is valid or not.
 Params:
   - fvi: pointer to the structure of volume to be checked.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

static int32_t __is_valid_vol( fsm_volinfo_t *fvi )
{
   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);

   /*
   if ( fvi->flag & FSM_MS_ATTACH )
      return os_set_errno(IONFS_EATTACH);
   */

   if ( fvi->flag & FSM_DEV_EJECTED )
      return os_set_errno(IONFS_EEJECT);

   return IONFS_OK;
}




/*
 Name: __fsm_sync
 Desc: Synchronize for write-access.
 Params:
   - fvi: pointer to the structure of volume to synchronize.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

static int32_t __fsm_sync( fsm_volinfo_t *fvi )
{
   int32_t rtn;


   if ( !(FSM_MS_DIRTY & fvi->flag) )
      return IONFS_OK;

   if ( 0 > ionFS_lock() )
      return -1;

   /* Synchronize the file system when data became dirty by mass-storage. */
   rtn = fvi->fs->op.sync( fvi->vol_id );
   fvi->flag &= ~(FSM_MS_DIRTY);

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: __get_valid_vol_id
 Desc: Get valid volume's id
 Params:
   - vol: The volume's name to get the id
 Returns:
   int32_t  >= 0 on sueess. The valid returned is the valid id to the given
                 volume's name.
            <  0 on fail.
 Caveats: None.
*/

static int32_t __get_valid_vol_id( const char_t *vol )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id;


   if ( 0 > (vol_id = __get_vol_id( vol )) )
      return os_set_errno(IONFS_EINVAL);

   fvi = GET_FS_VOL( vol_id );
   if ( 0 > __is_valid_vol( fvi ) )
      return -1;

   if ( 0 > __fsm_sync( fvi ) )
      return -1;

   return vol_id;
}




/*
 Name: __fsm_mark_dirty
 Desc: Change information of a volume to the DIRTY status.
 Params:
   - fvi: pointer to information of a volume.
 Returns: None
 Caveats: When data became dirty by mass-storage, the file system become DIRTY
          status.
*/

static void __fsm_mark_dirty( fsm_volinfo_t *fvi )
{
   fvi->flag |= FSM_FS_DIRTY;
}




/*
 Name: __fsm_zinit_fsm
 Desc: Zero initialize all data.
 Params: None.
 Returns: None.
 Caveats: None.
*/

static void __fsm_zinit_fsm( void )
{
   ionFS_memset( &fsm_vol_map, 0, sizeof(fsm_vol_map) );
   ionFS_memset( &fsm_mng, 0, sizeof(fsm_mng) );
   ionFS_memset( &fsm_vol, 0, sizeof(fsm_vol) );
   ionFS_memset( &fsm_inited, 0, sizeof(fsm_inited) );
}




/*
 Name: fat_zero_init
 Desc: Zero initialize all data.
 Params: None.
 Returns: None.
 Caveats: None.
*/

void fsm_zinit_fs( void )
{
   __fsm_zinit_fsm();
   fat_zero_init();
}




/*
 Name: fsm_init_fs
 Desc: Initialize the file system. This function must be called to
       use a file system.
 Params: None.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t fsm_init_fs( void )
{
   fsm_setupcallback_t *setup_callback = &fsm_setup_callback[0];
   fsm_mng_t *fsm = GET_FSM(0);
   fsm_volinfo_t *fvi;
   int32_t rtn,
           i;


   if ( true == fsm_inited )
      return IONFS_OK;

   /* Initialize to the map table of volume. */
   for ( i = 0; i < IONFS_VOLUME_NUM; i++ ) {
      fsm_vol_map[i].vol_id = -1;
      fsm_vol_map[i].dev_id = -1;
      fsm_vol_map[i].part_no= -1;
   }
   fsm_next_vol_id = 0;

   /* Initialize semaphores for file system. */
   if ( 0 > os_init_sm() )
      return os_get_errno();

   for ( i = 0; i < IONFS_FILESYSTEM_MAX; i++ ) {
      fsm[i].fs_id = i;
      fsm[i].flag = 0;

      /* Connect file system's foundational functions between FAT and FSM layer. */
      rtn = setup_callback[i]( &fsm[i].op );
      if ( IONFS_OK != rtn )  return os_set_errno( rtn );

      /* Call the low-level layer's initialization function. */
      rtn = fsm[i].op.init();
      if ( IONFS_OK != rtn )  return -1;

      fsm[i].flag = FSM_INITIALIZED;
   }

   /* Initialize information of volumes */
   for ( i = 0; i < IONFS_VOLUME_NUM; i++ ) {
      fvi = GET_FS_VOL(i);
      fvi->vol_id = -1;
      fvi->flag = 0;
      fvi->fs = (fsm_mng_t *) NULL;
   }

   /* Initialize file system in the LIM layer */
   if ( lim_init() < 0 )
      return -1;

   fsm_inited = true;

   return IONFS_OK;
}




/*
 Name: fsm_terminate
 Desc: Terminate the file system.
 Params: None.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: After this function is called, the file system can't use anymore.
*/

int32_t fsm_terminate( void )
{
   fsm_mng_t *fsm = GET_FSM(0);
   int32_t i;


   if ( false == fsm_inited )
      return os_set_errno( IONFS_ENOINIT );

   for ( i = 0; i < IONFS_FILESYSTEM_MAX; i++ )
      fsm[i].flag = 0;

   /* Finish semaphores for file system. */
   if ( 0 > os_terminate_sm() )
      return -1;

   /* Terminate file system in the LIM layer */
   if ( 0 > lim_terminate() )
      return -1;

   fsm_inited = false;
   return IONFS_OK;
}




/*
 Name: fsm_format
 Desc: Format a file system about a given volume.
 Params:
   - vol: The name of volume to be formatted.
   - dev_id: An ID of the device to be formatted.
   - part_no: A partition number to be formatted.
   - fs_type: A file system type. The type is one of the following string.
              "FAT6" - FAT16 system.
              "FAT32" - FAT32 system.
   - opt: A flag that describes which optional arguments are present. (Not
          supported.)
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: The mounted volume must be formatted after un-mount operation.
*/

int32_t fsm_format( const char_t *vol, uint32_t dev_id, uint32_t part_no, uint32_t start_sec,
                    uint32_t cnt, char_t *fs_type, uint32_t opt )
{
   #define BASE_LABEL "ionFS"
   fsm_mng_t *fsm;
   int32_t vol_id,
           rtn;
   char label[8],
        *p;
   uint32_t type;


   if ( NULL==vol )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __set_vol_id( vol, dev_id, part_no )) )
      return -1;

   /* Initialize the file system */
   if ( 0 > (rtn = fsm_init_fs()) )
      return os_set_errno(rtn);

   /* Create MBR partition. */
   if ( 0 > (rtn = mbr_creat_partition( dev_id, part_no, start_sec, cnt, fs_type )) )
      return rtn;

   if ( 0 > (rtn = lim_open( vol_id, dev_id, part_no ) ) )
      return rtn;

   if ( !ionFS_t_stricmp( FSNAME_FAT16, fs_type ) ) {
      fsm = GET_FSM(FSM_ID_FAT);
      type = eFAT16_SIZE;
   }
   else if ( !ionFS_t_stricmp( FSNAME_FAT32, fs_type ) ) {
      fsm = GET_FSM(FSM_ID_FAT);
      type = eFAT32_SIZE;
   }
   /*
   else if ( !ionFS_t_stricmp( FSNAME_NTFS, fs_type ) ) {
      fsm = GET_FSM(FSM_ID_NTFS);
      return os_set_errno( IONFS_EINVAL );
   }
   */
   else
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > ionFS_lock() )
      return -1;

   if ( !( fsm->flag & FSM_INITIALIZED ) )
      return os_set_errno(IONFS_ENOINIT);

   /* Make label for file system */
   p = label + lib_lstrcpy( label, BASE_LABEL );
   *p++ = (char)('0' + vol_id);
   *p = '\0';

   /* Call the real format routine. */
   rtn = fsm->op.format( vol_id, label, type );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_mount
 Desc: Mount a file system about a given volume.
 Params:
   - vol: The name of volume to be formatted. The name of volume can only
          contain the following characters: 'a' through 'z' regardless of
          character's case.
   - dev_id: An ID of the device to be mounted.
   - part_no: A partition number to be mounted.
   - opt: A flag that describes which optional arguments are present. (Not
          supported.)
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: The volumn sould be formmated for mounting the file system.
*/

int32_t fsm_mount( const char_t *vol, uint32_t dev_id, uint32_t part_no, uint32_t opt  )
{
   fsm_mng_t *fsm;
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           i,
           rtn;

	 
   if ( NULL == vol )
      return os_set_errno( IONFS_EINVAL );
	 
   if ( 0 > (vol_id = __set_vol_id( vol, dev_id, part_no )) ){   
		 return -1;
	 }
   /* Initialize the file system */
   if ( 0 > (rtn = fsm_init_fs()) )
      return os_set_errno(rtn);
	 
   if ( 0 > (rtn = lim_open( vol_id, dev_id, part_no ) ) ){
			 DEBUG_UART.print(rtn);
      return rtn;
	 }
	 
   fvi = GET_FS_VOL(vol_id);
   /* Returns error if the volume is already mounted. */
   if ( FSM_MOUNTED & fvi->flag )
      return os_set_errno( IONFS_EBUSY );
		 
   rtn = IONFS_ENOMNT;
	 
   if ( 0 > ionFS_lock() )
      return -1;

   for ( i = 0; i < IONFS_FILESYSTEM_MAX; i++ ) {
      fsm = GET_FSM(i);

      if ( !(FSM_INITIALIZED & fsm->flag) )
         return os_set_errno( IONFS_ENOINIT );

      /* Call the real mount routine. */
      rtn = fsm->op.mount( vol_id, opt );
      if ( IONFS_OK == rtn ) {
         fvi->vol_id = vol_id;
         fvi->flag |= FSM_MOUNTED;
         fvi->fs = fsm;
         break;
      }
      else
         rtn = -1;
   }

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_umount
 Desc: Un-mount a file system about a given volume.
 Params:
   - vol: The volume name of a mounted file system which is to be un-mounted.
   - opt: A flag that describes which optional arguments are present. (Not
          supported.)
 Returns:
   int32_t  0  on success.
           -1  on fail.
 Caveats: If a volume is in use, it cann't be unmounted.
*/

int32_t fsm_umount( const char_t *vol, uint32_t opt )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == vol )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( vol )) )
      return -1;

   if ( 0 > ionFS_lock() )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   /* Call the real un-mount routine. */
   rtn = fvi->fs->op.umount( vol_id, opt );
   if ( IONFS_OK == rtn )
      fvi->flag &= ~(uint32_t)FSM_MOUNTED;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_mkdir
 Desc: Make a specitic directory.
 Params:
   - path: A pointer to the null-terminated path name of the directory to be
           made.
   - mode: Reserved.
 Returns:
   iint32_t  0  on success.
            -1  on fail.
 Caveats: None.
*/

int32_t fsm_mkdir( const char_t *path, mod_t mode )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.mkdir( vol_id, &path[IONFS_VOL_NAME_LEN], mode );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_rmdir
 Desc: Remove a specitic directory.
 Params:
   - path: A pointer to the null-terminated path name of the directory to be
           removed.
 Returns:
   iint32_t  0  on success.
            -1  on fail.
 Caveats: If a directory has at least a file or a sub-directory, it cann't be
          removed.
*/

int32_t fsm_rmdir( const char_t *path )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.rmdir( vol_id, &path[IONFS_VOL_NAME_LEN] );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_opendir
 Desc: Open a specitic directory stream.
 Params:
   - path: A pointer to the null-terminated path name of the directory to be
           opened.
 Returns:
   DIR_t*  value on success. The value returned is a pointer to a DIR_t.
                       This DIR_t describes the directory.
                 NULL on fail.
 Caveats: None
*/

DIR_t* fsm_opendir( const char_t *path )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id;
   DIR_t *debuf;


   if ( NULL == path ) {
      os_set_errno( IONFS_EINVAL );
      return NULL;
   }

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return NULL;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return NULL;

   if ( NULL == (debuf = fvi->fs->op.opendir( vol_id, &path[IONFS_VOL_NAME_LEN] )) )
      return NULL;
   debuf->fd = SET_VOL_FD( vol_id, debuf->fd );

   if ( 0 > ionFS_unlock() )
      return NULL;

   return debuf;
}




/*
 Name: fsm_readdir
 Desc: Read a directory entry from a given directory stream.
 Params:
   - de: A pointer to a DIR_t that refers to the open directory stream to
         be read. This pointer is returned by ionFS_opendir().
 Returns:
   dirent_t*  value on success. The value returned is a pointer to a
                          dirent_t structure describing the next directory
                          entry in the directory stream.
                    NULL on fail or when the operation encounters the end of the
                         directory stream.
 Caveats: None.
*/

dirent_t *fsm_readdir( DIR_t *de )
{
   fsm_volinfo_t *fvi;
   dirent_t *dent;


   if ( NULL == de ) {
      os_set_errno( IONFS_EINVAL );
      return NULL;
   }

   if ( !IS_CORRECT_FD(de->fd) ) {
      os_set_errno(IONFS_EBADF);
      return NULL;
   }

   fvi = GET_FS_VOL( GET_VOL(de->fd) );

   if ( !( fvi->flag & FSM_MOUNTED ) ) {
      os_set_errno(IONFS_ENOMNT);
      return NULL;
   }

   if ( 0 > ionFS_lock() )
      return NULL;

   if ( NULL == (dent = fvi->fs->op.readdir( de )) )
      return NULL;

   if ( 0 > ionFS_unlock() )
      return NULL;

   return dent;
}




/*
 Name: fsm_rewinddir
 Desc: Set the directory position to the beginning of the directory entries in
       directory stream.
 Params:
   - de: A pointer to a DIR_t that refers to the open directory stream to
         be rewound. This pointer is returned by the ionFS_opendir() function.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_rewinddir( DIR_t *de )
{
   fsm_volinfo_t *fvi;
   int32_t rtn;


   if ( NULL == de )
      return os_set_errno( IONFS_EINVAL );

   if ( !IS_CORRECT_FD(de->fd) )
      return os_set_errno(IONFS_EBADF);

   fvi = GET_FS_VOL( GET_VOL(de->fd) );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.rewinddir( de );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_closedir
 Desc: Close a specific directory stream.
 Params:
   - de: A pointer to a DIR_t to be closed. This pointer is returned by
         the ionFS_opendir() function.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_closedir( DIR_t *de )
{
   fsm_volinfo_t *fvi;
   int32_t rtn;


   if ( NULL == de )
      return os_set_errno( IONFS_EINVAL );

   if ( !IS_CORRECT_FD(de->fd) )
      return os_set_errno(IONFS_EBADF);

   fvi = GET_FS_VOL( GET_VOL(de->fd) );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.closedir( de );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_cleandir
 Desc: Delete all files in directory. (except directory)
 Params:
   - path: A pointer to the null-terminated path name of the directory to be
           cleaned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_cleandir( const char_t *path )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.cleandir( vol_id, &path[IONFS_VOL_NAME_LEN] )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_statdir
 Desc: Get information of a specific directory.
 Params:
   - path: A pointer to the null-terminated path name of the directory to be
           researched.
   - statbuf: Pointer to an object of type struct statdir_t where the
              file information will be written.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_statdir( const char_t *path, statdir_t *statbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.statdir( vol_id, &path[IONFS_VOL_NAME_LEN], statbuf )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_access
 Desc: Check for file's accessibility.
 Params:
   - path: A pointer to the null-terminated path name of the file or directory
           to be checked.
   - amode: Bitwise OR of the access permissions to be checked. The file's
            access permission to be checked is one of the following symbols.
            ION_R_OK - Read authority
            ION_W_OK - Write authority
            ION_X_OK - Execution authority.
            ION_F_OK - File existence.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_access( const char_t *path, int32_t amode )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.access( vol_id, &path[IONFS_VOL_NAME_LEN], amode )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_creat
 Desc: Create a specific file as given mode.
 Params:
   - path: A pointer to the null-terminated path name of the file to be opened.
   - mode: The file mode indicates the file permission. (Not supported.)
 Returns:
   int32_t  >=0 on success. The value returned is the file descriptor.
            < 0 on fail.
 Caveats: The function truncate the file size to 0 when same name files exist
          there.
*/

int32_t fsm_creat( const char_t *path, mod_t mode )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.creat( vol_id, &path[IONFS_VOL_NAME_LEN], mode );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   if ( 0 > rtn ) return rtn;
   return SET_VOL_FD( vol_id, rtn );
}




/*
 Name: fsm_fopen
 Desc: Open or create a specific file as given the mode.
 Params:
   - path: A pointer to the null-terminated path name of the file to be opened.
   - flag: The status flags and access modes of the file to be opened.
           The flag is one of the following symbols.
           ION_O_RDONLY - open file for reading only file
           ION_O_WRONLY - open file for writing only file
           ION_O_RDWR - open file for both reading and writing.
           ION_O_APPEND - open file with position the file offset at the end of the
                      file. Before each write operation, file position is set
                      at the end of the file.
           ION_O_CREAT - If the file being opened does not exist, it is created and
                     then opened. If the file being opened exists, this mode is
                     no effect.
           ION_O_TRUNC - Truncate the file to zero length if the file exists.
           ION_O_EXCL - Ignored if ION_O_CREAT is not set. It causes the call to
                    ionFS_open() to fail if the file already exists.
   - mode: Permission bits to use if a file is created. (Ignored)
 Returns:
   int32_t  >=0 on success. The value returned is the file descriptor.
            < 0 on fail.
 Caveats: There directory which is used for the path or files should exist.
          When a file is opened, flag is able to be used with making up each
          other through OR operator.
*/

int32_t fsm_fopen( const char_t *path, uint32_t flag, ... /* mod_t mode */ )
{
   fsm_volinfo_t *fvi;
   mod_t mode;
   int32_t vol_id,
           rtn;
   va_list ap;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   va_start(ap, flag);
   mode = (mod_t) va_arg(ap, uint32_t);
   va_end(ap);

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.open( vol_id, &path[IONFS_VOL_NAME_LEN], flag, mode );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   if ( 0 > rtn ) return rtn;
   return SET_VOL_FD( vol_id, rtn );
}




/*
 Name: fsm_read
 Desc: Read from a specific file.
 Params:
   - fd: The file descriptor to be read.
   - buf: A pointer to a buffer in which the bytes read are placed.
   - bytes: The number of bytes to be read.
 Returns:
   ssiz_t  >=0 on success. The value returned is the number of bytes actually
               read and placed in buf. If the number of bytes to be read was
               larger than the remained size of file at the current position of
               file, it may be less than bytes.
           < 0 on fail.
 Caveats: If file pointer points to the first of the file and file size is
          smaller than the requested read sizee, file is read as much as file
          size and it lets file pointer move as much as  file was read.
          If the requested read size is larger than data size which remained
          data at the postion of file pointer, this function  would read the
          data as much as remained data.
*/

ssiz_t fsm_read( int32_t fd, void *buf, siz_t bytes )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   /* Check the status of fd */
   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   /* Get volume's ID. Exist file in volume */
   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   /* Get volume maneger's pointer */
   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );


   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.read( GET_FD(fd), buf, bytes )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
Name: fsm_write
Desc: Write to a specific file.
Params:
   - fd: The descriptor of the file to which the data is to be written.
   - buf: A pointer to a buffer containing the data to be written.
   - bytes: The size in bytes of the data to be written.
Returns:
   ssiz_t  >=0 on success. The value returned is the number of bytes actually
               written. This number is less than or equal to bytes.
           < 0 on fail.
Caveats: If the file descriptor have the ION_O_APPEND mode, data would be written
         from the end of file. Or not, data would be written from where file
         pointer point to. Afer file is written, File pointer would be moved
         as much as dada was written
*/

ssiz_t fsm_write( int32_t fd, const void *buf, siz_t bytes )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);


   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.write( GET_FD(fd), buf, bytes );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_lseek
 Desc: Change the position of file pointer in the file which the 'fd' parameter
       point to.
 Params:
   - fd: The file descriptor whose current file offset you wanted to change.
   - offset: The amount byte offset is to be changed. The sign indicates
             whether the offset is to be moved forward (positive) or backward
             (negative).
   - whence: One of the following symbols.
             ION_SEEK_SET: The start of the file.
             ION_SEEK_CUR: The current file offset in the file.
             ION_SEEK_END: The end of the file.
 Returns:
   offs_t  >=0 on success. The value returned is the new file offset, measured
               in bytes from the beginning of the file.
           < 0 on fail.
 Caveats: ionFS_lseek(fd, 0, ION_SEEK_CUR) is equal to ionFS_tell(fd).
*/

offs_t fsm_lseek( int32_t fd, offs_t offset, int32_t whence  )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);


   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.lseek( GET_FD(fd), offset, whence )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return (offs_t) rtn;
}




/*
 Name: fsm_fsync
 Desc:
 Params:
   - fd: The file descriptor whose current file offset you wanted to change.
 Returns:
   offs_t  =0 on success.
           <0 on fail.
 Caveats:
*/

int32_t fsm_fsync( int32_t fd  )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno(IONFS_ENOMNT);


   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.fsync( GET_FD(fd) )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_fclose
 Desc: Close a specific file.
 Params:
   - fd: The file descriptor to be closed.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_fclose( int32_t fd )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.close( GET_FD(fd) );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_closeall
 Desc: Close all files in a specific volume.
 Params:
   - vol: The volume name which all files are closed.
 Returns:
   int32_t  0 on success.
           -1 on fail.
  Caveats: Before the file is unlinked, it must be closed.
*/

int32_t fsm_closeall( const char_t *vol )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == vol )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( vol )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.closeall( vol_id );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_unlink
 Desc: Remove a specific file.
 Params:
   - path: A pointer to the null-terminated path name of the file to be
           unlinked.
 Returns:
    int32_t  0 on success.
            -1 on fail.
 Caveats: Before the file removes, it must be closed. This function cann't
          remove a directory entry.
*/

int32_t fsm_unlink( const char_t *path )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.unlink( vol_id, &path[IONFS_VOL_NAME_LEN] );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_truncate
 Desc: Truncate a specific file to a new size.
 Params:
   - fd: The descriptor of the file to which the length is to be changed.
   - new_size: The new length of the file in bytes.
 Returns:
    int32_t  0 on success.
            -1 on fail.
 Caveats: None.
*/

int32_t fsm_truncate( int32_t fd, siz_t new_size )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );


   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.truncate( GET_FD(fd), new_size );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_tell
 Desc: Get the current position of a specific opend file.
 Params:
   - fd: The file descriptor whose current file offset you wanted to obtain.
 Returns:
   int32_t  >=0 on success. The value returned is the current file offset,
                 measured in bytes from the beginning of the file.
            < 0 on fail.
 Caveats: ionFS_tell(fd) is equal to ionFS_lseek(fd, 0, ION_SEEK_CUR).
 */

int32_t fsm_tell( int32_t fd )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.tell( GET_FD(fd) )) )
      return -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_rename
 Desc: Change the name of a specific file or directory.
 Params:
   - oldpath: A pointer to the path name of the file or directory to be renamed.
   - newpath: A pointer to the new path name of the file or directory.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_rename( const char_t *oldpath, const char_t *newpath )
{
   fsm_volinfo_t *fvi;
   int32_t dwOldVolID,
           dwNewVolID,
           rtn;


   if ( NULL == oldpath || NULL == newpath )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (dwOldVolID = __get_valid_vol_id( oldpath )) )
      return -1;
   if ( 0 > (dwNewVolID = __get_valid_vol_id( newpath )) )
      return -1;

   if ( dwOldVolID != dwNewVolID )
      return os_set_errno( IONFS_EXDEV );


   fvi = GET_FS_VOL(dwOldVolID);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.rename( dwOldVolID, &oldpath[IONFS_VOL_NAME_LEN],
                           &newpath[IONFS_VOL_NAME_LEN] );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_stat
 Desc: Get information in a specific file or directory by using a file path
       name.
 Params:
   - path: A pointer to the null-terminated path name of the file or directory
           obtain information.
   - statbuf: A pointer to a buffer of type struct ionFS_stat_t where file
              status information is returned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_stat( const char_t *path, stat_t *statbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path || NULL == statbuf )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.stat( vol_id, &path[IONFS_VOL_NAME_LEN], statbuf )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_fstat
 Desc: Get information in a specific file or directory by using a file
       descriptor.
 Params:
   - fd: A file descriptor of the file or directory to obtain information.
   - statbuf: A pointer to a buffer of type struct ionFS_stat_t where file
              status information is returned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_fstat( int32_t fd, stat_t *statbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.fstat( GET_FD(fd), statbuf )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_getattr
 Desc: Get an attribute in a specific file or directory by using a file path
       name.
 Params:
   - path: A pointer to the null-terminated path name of the file or directory
           to obtain an attribute.
   - attr: A pointer to a buffer where an attribute is returned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_getattr( const char_t *path, uint32_t *attrbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path && NULL == attrbuf )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.getattr( vol_id, &path[IONFS_VOL_NAME_LEN], attrbuf )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_fgetattr
 Desc: Get an attribute in a specific file or directory by using a file
       descriptor.
 Params:
   - fd: A file descriptor of the file or directory to obtain an attribute.
   - attr: A pointer to a buffer where an attribute is returned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_fgetattr( int32_t fd, uint32_t *attrbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.fgetattr( GET_FD(fd), attrbuf )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_setattr
 Desc: Set an attribute in a specific file or directory by using a file path
       name.
 Params:
   - path: A pointer to the null-terminated path name of the file or directory
           to obtain an attribute.
   - attr:  The attribute of file to be set. It is able to be the following
            symbols and be used with making up each other through OR operator.
            FA_RDONLY - Specifies a file for reading only.
            FA_HIDDEN - Specifies a hidden file.
            FA_SYSTEM - Specifies a system file.
            FA_ARCHIVE - Specifies a archive file.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_setattr( const char_t *path, uint32_t attr )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == path )
      return os_set_errno( IONFS_EINVAL );

   /* If the attribute of the file is in the read-only or hidden or system or
      archive, it can't set the attribute */
   if ( 0 == (attr & (FA_RDONLY | FA_HIDDEN | FA_SYSTEM | FA_ARCHIVE)) )
      return os_set_errno( IONFS_EINVAL );
   attr &= (FA_RDONLY | FA_HIDDEN | FA_SYSTEM | FA_ARCHIVE);

   if ( 0 > (vol_id = __get_valid_vol_id( path )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.setattr( vol_id, &path[IONFS_VOL_NAME_LEN], attr );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      return -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_fsetattr
 Desc: Set an attribute in a specific file or directory by using a file
       descriptor.
 Params:
   - fd: A file descriptor of the file or directory to set an attribute.
   - attr: The attribute of file to be set. It is able to be the following
            symbols and be used with making up each other through OR operator.
            FA_RDONLY - Specifies a file for reading only.
            FA_HIDDEN - Specifies a hidden file.
            FA_SYSTEM - Specifies a system file.
            FA_ARCHIVE - Specifies a archive file.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None.
*/

int32_t fsm_fsetattr( int32_t fd, uint32_t attr )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( !IS_CORRECT_FD(fd) )
      return os_set_errno(IONFS_EBADF);

   /* If the attribute of the file is in the read-only or hidden or system or
      archive, it can't set the attribute */
   if ( 0 == (attr & (FA_RDONLY | FA_HIDDEN | FA_SYSTEM | FA_ARCHIVE)) )
      return os_set_errno( IONFS_EINVAL );
   attr &= (FA_RDONLY | FA_HIDDEN | FA_SYSTEM | FA_ARCHIVE);

   vol_id = GET_VOL(fd);
   if ( 0 > fd || IONFS_VOLUME_NUM < vol_id )
      return os_set_errno( IONFS_EINVAL );

   fvi = GET_FS_VOL( vol_id );

   if ( !( fvi->flag & FSM_MOUNTED ) )
      return os_set_errno( IONFS_ENOMNT );

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = fvi->fs->op.fsetattr( GET_FD(fd), attr );
   __fsm_mark_dirty( fvi );

   if ( 0 > rtn )
      return -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_sync
 Desc: Synchronize the cache data in file system and physical data.
 Params:
   - vol: The volume name of the file system which is to be synchronized.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None
*/

int32_t fsm_sync( const char_t *vol )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == vol )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( vol )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   if ( 0 > (rtn = fvi->fs->op.sync( vol_id )) )
      rtn = -1;

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_statfs
 Desc: Get information of the mounted volume.
 Params:
   - vol: The volume name of the file system which is to obtain information.
   - statbuf: A pointer to a buffer of statfs_t struct type where file
              system status information is returned.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: None
*/

int32_t fsm_statfs( const char_t *vol, statfs_t *statbuf )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn;


   if ( NULL == vol || NULL == statbuf )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_valid_vol_id( vol )) )
      return -1;

   fvi = GET_FS_VOL(vol_id);

   if ( 0 > ionFS_lock() )
      return -1;

   statbuf->fs_id = fvi->fs->fs_id;
   rtn = fvi->fs->op.statfs( vol_id, statbuf );

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_ioctl
 Desc: Control I/O status.
 Params:
   - vol: The name of volume which is changed the I/O status.
   - func: The requested function. The function is one of the following symbols.
           IO_MS_ATTACH - Requests attach of a mass-storage.
           IO_MS_DETACH - Requests detach of a mass-storage.
           IO_DEV_ATTACH - Requests attach of a device.
           IO_DEV_DETACH - Requests detach of a device.
           IO_PART_INFO - Requests the partition's information.
           IO_OP - Requests the following operation: read, write, erase.
           IO_READ - Requests read function.
           IO_WRITE - Requests write function.
           IO_ERASE - Requests erase function.
   - param: Pointer to the parameter to be used by function.
 Returns:
   int32_t  0 on success.
           -1 on fail.
 Caveats: In this version, fsm_ioctl() function doesn't support. So, This
          function do nothing.
*/

int32_t fsm_ioctl( const char_t *vol, uint32_t func, void *param )
{
   fsm_volinfo_t *fvi;
   int32_t vol_id,
           rtn = IONFS_OK;


   if ( NULL == vol )
      return os_set_errno( IONFS_EINVAL );

   if ( 0 > (vol_id = __get_vol_id( vol )) )
      return -1;

   fvi = GET_FS_VOL( vol_id );

   if ( IO_OP & func ) {
      if ( 0 > __is_valid_vol( fvi ) )
         return -1;
   }

   /* Check whether ionFS wrote some data or not. If so, the mass-storage can't
      write anything in memory. */
   if ( IO_WRITE == func && FSM_FS_DIRTY & fvi->flag )
      return IONFS_OK;

   if ( !( fvi->flag & FSM_MOUNTED ) ) {
      os_set_errno(IONFS_ENOMNT);
      return -1;
   }

   if ( 0 > ionFS_lock() )
      return -1;

   rtn = lim_ioctl( vol_id, func, param );

   switch ( func ) {
      case IO_MS_ATTACH:
         fvi->flag |= FSM_MS_ATTACH;
         fvi->flag &= ~(FSM_FS_DIRTY);
         break;
      case IO_MS_DETACH:
         fvi->flag &= ~(FSM_MS_ATTACH | FSM_MOUNTED);
         break;
      case IO_WRITE:
         fvi->flag |= FSM_MS_DIRTY;
         break;
   }

   if ( 0 > ionFS_unlock() )
      return -1;

   return rtn;
}




/*
 Name: fsm_set_safe_mode
 Desc: Control the safe mode.
 Params:
   - issafe: the flag value for the safe mode.
             true - Set the safe mode on.
             false - Set the safe mode off.
 Returns:
   - boot_t  true means that the safe mode was set.
             false means that the safe mode wasn't set.
 Caveats: The purpose of safe mode is debugging. In the safe mode, the file
          system never use the resource of operating system.
*/

bool_t fsm_set_safe_mode( bool_t issafe )
{
   return os_set_safe_mode( issafe );
}




/*
 Name: fsm_get_sectors
 Desc: Get total sectors from a specific device.
 Params:
   - dev_id: An ID of the device to get a number of sectors from the memory.
 Returns:
   int32_t  >=0 on success. The value returned means a number of sectors in
                the memory.
            < 0 on fail.
 Caveats: None
*/

int32_t fsm_get_sectors( int32_t dev_id )
{
   int32_t rtn;

   if ( IONFS_DEVICE_NUM <= dev_id ) {
      os_set_errno( IONFS_EINVAL );
      return -1;
   }

   rtn = pim_open( dev_id );
   if ( 0 > rtn )
      return rtn;

   return pim_get_sectors( dev_id );
}




/*
 Name: fsm_get_devicetype
 Desc: Get name of specific device.
 Params:
   - dev_id: An ID of the device to get a number of sectors from the memory.
   - name: Device type name
 Returns:
   int32_t  0 on success. The value returned means that ID's name is valid.
            < 0 on fail.
 Caveats: None.
*/

int32_t fsm_get_devicetype( int32_t dev_id, char_t * name )
{
   int32_t rtn;

   if ( IONFS_DEVICE_NUM <= dev_id ) {
      os_set_errno( IONFS_EINVAL );
      return -1;
   }

   rtn = pim_open( dev_id );
   if ( 0 > rtn )
      return rtn;

   return pim_get_devicetype( dev_id, name );
}




/*
 Name: fsm_get_version
 Desc: Get version of the 'ionFS'.
 Params: None.
 Returns:
   fsver_t*  value on success. The pointer returned has information of current
             file system.
 Caveats: None.
*/

const fsver_t *fsm_get_version( void )
{
   const static fsver_t vernum = {
      "SpansionFS",
      1,
      2,
      8 };

   return &vernum;
}
/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/

