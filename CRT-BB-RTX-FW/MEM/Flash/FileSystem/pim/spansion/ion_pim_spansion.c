/* FILE: ion_pim_spansion.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "ion_global.h"

#if ( IONFS_BD == IONFS_BD_SPANSION )

#include "ion_pim_spansion.h"
#include "ftl_if_ex.h"

#if ( IONFS_DEVICE_NUM > 1 ) 
#include "DEV_1_ftl_if_ex.h"
#endif

#include "Flash.h"
#include "BlueBrain_IO.h"

extern bool_t pim_dev_inited[];
uint8_t ftl_initialized[IONFS_DEVICE_NUM] = {0};



/***********************************************************************/
/*
 Name: __convert_ionFS_status
 Desc:
 Params:
   - ftl_status:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

static int32_t __convert_ionFS_status( FTL_STATUS ftl_status )
{
    int32_t status;

    switch(ftl_status)
    {
       case FTL_ERR_PASS:
          status = IONFS_OK;
          break;

       default:
					DBG_Printf("ERROR: FTL return code = %u\n", ftl_status, 0);
          status = IONFS_EIO;
          break;
    }

    return status;
}




/*
 Name: pim_setup_span
 Desc:
 Params:
   - de:
   - name:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_setup_span( pim_devinfo_t *de, char_t * name )
{
    if(de == NULL)
    {
       return IONFS_EINVAL;
    }

    de->dev_flag = ePIM_NeedErase;

    de->op.ioctl = pim_ioctl_span;
    de->op.read_sector = pim_readsector_span;
    de->op.write_sector = pim_writesector_span;
    de->op.erase_sector = pim_erasesector_span;

    de->dev_name = name;

    return IONFS_OK;
}




/*
 Name: __pim_ioctl_span_format
 Desc:
 Params:
   - dev_id: Device's id.
 Returns:
   int32_t  0(=IONFS_OK) always.
 Caveats: None.
*/

static int32_t __pim_ioctl_span_format( int32_t dev_id  )
{
    FTL_INIT_STRUCT initStruct;
    FTL_STATUS status;
    #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
    initStruct.format = FTL_DONT_FORMAT;
    initStruct.os_type = FTL_RTOS_INT;        /* not use */
    initStruct.table_storage = FTL_TABLE_NV;  /* not use */
    initStruct.allocate = FTL_ALLOCATE;  /* not use */
    initStruct.mode = 0;
	initStruct.total_ram_allowed = gTotal_ram_allowed;
    #endif
    #if ( IONFS_DEVICE_NUM > 1 ) 
    DEV_1_FTL_INIT_STRUCT DEV_1_initStruct;
    #endif

    switch(dev_id)
    {
    case IONFS_SPAN_DEVA:


       #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
       if ((status = FTL_Format(&initStruct)) != FTL_ERR_PASS)
       #else
       if((status = FTL_Format()) != FTL_ERR_PASS)
       #endif
       {
           DBG_Printf("ERROR: __pim_ioctl_span_format: FTL_Format() \n", 0, 0);
           return __convert_ionFS_status(status);
       }
       initStruct.format = FTL_DONT_FORMAT;       // Don't Format
       initStruct.os_type = FTL_RTOS_INT;         // Use Blocking Code
       initStruct.table_storage = FTL_TABLE_LOCATION;
       initStruct.allocate = FTL_ALLOCATE;        // Allocate tables
       #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
	   initStruct.total_ram_allowed = gTotal_ram_allowed;
       #endif
       if((status = FTL_InitAll(&initStruct)) != FTL_ERR_PASS)
       {
					DBG_Printf("ERROR: __pim_ioctl_span_format: FTL_InitAll() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       break;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    case IONFS_SPAN_DEVB:
       /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
       #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
       if ((status = DEV_1_FTL_Format(&initStruct)) != FTL_ERR_PASS)
       #else
       if((status = DEV_1_FTL_Format()) != FTL_ERR_PASS)
       #endif
       {
					DBG_Printf("ERROR: __pim_ioctl_span_format: DEV_1_FTL_Format() \n", 0, 0);
           return __convert_ionFS_status(status);
       }
       DEV_1_initStruct.format = FTL_DONT_FORMAT;       // Don't Format
       DEV_1_initStruct.os_type = FTL_RTOS_INT;         // Use Blocking Code
       DEV_1_initStruct.table_storage = FTL_TABLE_LOCATION;
       DEV_1_initStruct.allocate = FTL_ALLOCATE;        // Allocate tables
       #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
	   DEV_1_initStruct.total_ram_allowed = gTotal_ram_allowed;
       #endif
       /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
       if((status = DEV_1_FTL_InitAll(&DEV_1_initStruct)) != FTL_ERR_PASS)
       {
					DBG_Printf("ERROR: __pim_ioctl_span_format: DEV_1_FTL_InitAll() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       break;
     #endif
     default:
         return __convert_ionFS_status(FTL_ERR_FAIL);
    }
    ftl_initialized[dev_id] = 1;
    return IONFS_OK;
}




/*
 Name: pim_ioctl_span_init
 Desc:
 Params:
   - dev_id: Device's id.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_ioctl_span_init( int32_t dev_id )
{
    FTL_INIT_STRUCT initStruct;
    FTL_STATUS status;
    #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
    initStruct.format = FTL_DONT_FORMAT;
    initStruct.os_type = FTL_RTOS_INT;        /* not use */
    initStruct.table_storage = FTL_TABLE_NV;  /* not use */
    initStruct.allocate = FTL_ALLOCATE;  /* not use */
    initStruct.mode = 0;
	initStruct.total_ram_allowed = gTotal_ram_allowed;
    #endif
    #if ( IONFS_DEVICE_NUM > 1 ) 
    DEV_1_FTL_INIT_STRUCT DEV_1_initStruct;
    #endif

    if(pim_dev_inited[dev_id] == true)
    {
       return IONFS_OK;
    }
    switch(dev_id)
    {
    case IONFS_SPAN_DEVA:
        if(ftl_initialized[dev_id] == 0)
        {
            initStruct.format = FTL_DONT_FORMAT;       // Format as needed
            initStruct.os_type = FTL_RTOS_INT;         // Use Blocking Code
            initStruct.table_storage = FTL_TABLE_LOCATION;
            initStruct.allocate = FTL_ALLOCATE;        // Allocate tables
            #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
			initStruct.total_ram_allowed = gTotal_ram_allowed;
            #endif

            if((status = FTL_InitAll(&initStruct)) != FTL_ERR_PASS)
            {
		        if(status == FTL_ERR_NOT_FORMATTED)
		        {
                    #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
                    if ((status = FTL_Format(&initStruct)) != FTL_ERR_PASS)
                    #else
                    if ((status = FTL_Format()) != FTL_ERR_PASS)
                    #endif
			        {
                       DBG_Printf("ERROR: pim_ioctl_span_init: FTL_Format() \n", 0, 0);
                        return __convert_ionFS_status(status);
			        }
                    if((status = FTL_InitAll(&initStruct)) != FTL_ERR_PASS)
                    {
											printf("Status : %d", status );
													DBG_Printf("ERROR: pim_ioctl_span_init: FTL_InitAll() 2\n", 0, 0);			
                         return __convert_ionFS_status(status);
 	                }
		       }
		       else
	           {
										DBG_Printf("ERROR: pim_ioctl_span_init: FTL_InitAll() \n", 0, 0);
                   return __convert_ionFS_status(status);
	           }

           }
           ftl_initialized[dev_id] = 1;
        }
        break;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    case IONFS_SPAN_DEVB:
        if(ftl_initialized[dev_id] == 0)
        {
            DEV_1_initStruct.format = FTL_DONT_FORMAT;       // Format as needed
            DEV_1_initStruct.os_type = FTL_RTOS_INT;         // Use Blocking Code
            DEV_1_initStruct.table_storage = FTL_TABLE_LOCATION;
            DEV_1_initStruct.allocate = FTL_ALLOCATE;        // Allocate tables
            #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
			DEV_1_initStruct.total_ram_allowed = gTotal_ram_allowed;
            #endif
            /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
            if((status = DEV_1_FTL_InitAll(&DEV_1_initStruct)) != FTL_ERR_PASS)
            {
		        if(status == FTL_ERR_NOT_FORMATTED)
		        {
                    /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
                    #if (CACHE_RAM_BD_MODULE == FTL_TRUE && CACHE_DYNAMIC_ALLOCATION == FTL_TRUE)
                    if((status = DEV_1_FTL_Format(&initStruct)) != FTL_ERR_PASS)
                    #else
                    if((status = DEV_1_FTL_Format()) != FTL_ERR_PASS)
                    #endif
			        {
                      DBG_Printf("ERROR: pim_ioctl_span_init: DEV_1_FTL_Format() \n", 0, 0);
                        return __convert_ionFS_status(status);
			        }
                    /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
                    if((status = DEV_1_FTL_InitAll(&DEV_1_initStruct)) != FTL_ERR_PASS)
                    {
                        DBG_Printf("ERROR: pim_ioctl_span_init: DEV_1_FTL_InitAll() 2\n", 0, 0);
                         return __convert_ionFS_status(status);
 	                }
		       }
		       else
	           {
                  DBG_Printf("ERROR: pim_ioctl_span_init: DEV_1_FTL_InitAll() \n", 0, 0);
                   return __convert_ionFS_status(status);
	           }

           }
           ftl_initialized[dev_id] = 1;
        }
        break;
    #endif
        default:
          return __convert_ionFS_status(FTL_ERR_FAIL);            
    }
    pim_dev_inited[dev_id] = true;
//		printf("Reached here");
    return IONFS_OK;
}




/*
 Name: __pim_ioctl_span_terminate
 Desc:
 Params:
   - dev_id: Device's id.
 Returns:
   int32_t  0(=IONFS_OK) always.
 Caveats: None.
*/

static int32_t __pim_ioctl_span_terminate( int32_t dev_id )
{
    FTL_STATUS status;
    switch(dev_id)
    {
    case IONFS_SPAN_DEVA:
       if((status = FTL_Shutdown()) != FTL_ERR_PASS)
       {
         DBG_Printf("ERROR: __pim_ioctl_span_terminate: FTL_Shutdown() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       break;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    case IONFS_SPAN_DEVB:
       /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
       if((status = DEV_1_FTL_Shutdown()) != FTL_ERR_PASS)
       {
         DBG_Printf("ERROR: __pim_ioctl_span_terminate: DEV_1_FTL_Shutdown() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       break;
    #endif 
    default:
        return __convert_ionFS_status(FTL_ERR_FAIL);            
    }
    ftl_initialized[dev_id] = 0;
    pim_dev_inited[dev_id] = false;
    return IONFS_OK;
}




/*
 Name: __pim_ioctl_span_open
 Desc:
 Params:
   - dev_id: Device's id.
   - de:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

static int32_t __pim_ioctl_span_open( int32_t dev_id, pim_devinfo_t *de )
{
    FTL_STATUS status;
    FTL_CAPACITY cap;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    DEV_1_FTL_CAPACITY DEV_1_cap;
    #endif
    if(de == NULL)
    {
       return IONFS_EINVAL;
    }

    if(de->opened == true)
    {
       return IONFS_OK;
    }
    switch(dev_id)
    {
    case IONFS_SPAN_DEVA:
       if((status = FTL_GetCapacity(&cap)) != FTL_ERR_PASS)
       {
          DBG_Printf("ERROR: __pim_ioctl_span_open: FTL_GetCapacity() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       de->totsect_cnt = cap.numBlocks;
       de->sects_per_block = cap.blockSize / IONFS_SPAN_SECTOR_SIZE;

	   break;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    case IONFS_SPAN_DEVB:
       /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
       if((status = DEV_1_FTL_GetCapacity(&DEV_1_cap)) != FTL_ERR_PASS)
       {
         DBG_Printf("ERROR: __pim_ioctl_span_open: DEV_1_FTL_GetCapacity() \n", 0, 0);
          return __convert_ionFS_status(status);
       }
       de->totsect_cnt = DEV_1_cap.numBlocks;
       de->sects_per_block = DEV_1_cap.blockSize / IONFS_SPAN_SECTOR_SIZE;

       break; 
    #endif 
    default:
          return __convert_ionFS_status(FTL_ERR_FAIL);            
    }

    de->start_sect = IONFS_SPAN_START_SECTOR;
    de->end_sect = de->totsect_cnt - 1;
    de->bytes_per_sect = IONFS_SPAN_SECTOR_SIZE;
    de->bits_per_sectsize = IONFS_SPAN_SECTOR_SIZE_BITS;  // log2(bytes_per_sect)

	return IONFS_OK;
}




/*
 Name: __pim_ioctl_span_close
 Desc:
 Params:
   - dev_id: Device's id.
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

static int32_t __pim_ioctl_span_close( int32_t dev_id )
{
    /* NULL */
    return IONFS_OK;
}




/*
 Name: pim_ioctl_span
 Desc:
 Params:
   - dev_id: Device's id.
   - cmd:
   - arg:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_ioctl_span( int32_t dev_id, pim_ioctl_cmd_t cmd, void *arg )
{
    switch(cmd)
    {
       case eIOCTL_format:
          return __pim_ioctl_span_format(dev_id);

       case eIOCTL_init:
          return pim_ioctl_span_init(dev_id);

       case eIOCTL_terminate:
          return __pim_ioctl_span_terminate(dev_id);

       case eIOCTL_open:
          return __pim_ioctl_span_open(dev_id, (pim_devinfo_t *)arg);

       case eIOCTL_close:
          return __pim_ioctl_span_close(dev_id);
    }

    return IONFS_EINVAL;
}




/*
 Name: pim_readsector_span
 Desc:
 Params:
   - dev_id: Device's id.
   - sect_no:
   - buf:
   - cnt:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_readsector_span( int32_t dev_id, uint32_t sect_no, uint8_t *buf, uint32_t cnt )
{
    FTL_STATUS status;
    unsigned long done;
    switch(dev_id)
    {
        case IONFS_SPAN_DEVA:
        if((status = FTL_DeviceObjectsRead(buf, sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
           DBG_Printf("ERROR: pim_readsector_span: sect_no=0x%X, ", sect_no, 0);
           DBG_Printf("cnt=%d\n", cnt, 0);
           return __convert_ionFS_status(status);
        }
        break;
        #if ( IONFS_DEVICE_NUM > 1 ) 
        case IONFS_SPAN_DEVB:
       /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
        if((status = DEV_1_FTL_DeviceObjectsRead(buf, sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
           DBG_Printf("ERROR: DEV_1 pim_readsector_span: sect_no=0x%X, ", sect_no, 0);
           DBG_Printf("cnt=%d\n", cnt, 0);
           return __convert_ionFS_status(status);
        }
        break;
        #endif
        default:
           return __convert_ionFS_status(FTL_ERR_FAIL);            
    }
    return IONFS_OK;
}




/*
 Name: pim_writesector_span
 Desc:
 Params:
   - dev_id: Device's id.
   - sect_no:
   - buf:
   - cnt:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_writesector_span( int32_t dev_id, uint32_t sect_no, uint8_t *buf, uint32_t cnt )
{
    FTL_STATUS status;
    unsigned long done;
    switch(dev_id)
    {
        case IONFS_SPAN_DEVA:
        if((status = FTL_DeviceObjectsWrite(buf, sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
            DBG_Printf("ERROR: pim_writesector_span: sect_no=0x%X, ", sect_no, 0);
						DBG_Printf("cnt=%d\n", cnt, 0);
            return __convert_ionFS_status(status);
        }
        break;
        #if ( IONFS_DEVICE_NUM > 1 ) 
        case IONFS_SPAN_DEVB:
        /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
        if((status = DEV_1_FTL_DeviceObjectsWrite(buf, sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
            DBG_Printf("ERROR: DEV_1 pim_writesector_span: sect_no=0x%X, ", sect_no, 0);
            DBG_Printf("cnt=%d\n", cnt, 0);
            return __convert_ionFS_status(status);
        }
        break;
        #endif
        default:
           return __convert_ionFS_status(FTL_ERR_FAIL);            
    }
    return IONFS_OK;
}




/*
 Name: pim_erasesector_span
 Desc:
 Params:
   - dev_id: Device's id.
   - sect_no:
   - cnt:
 Returns:
   int32_t  =0 on success.
            <0 on fail.
 Caveats: None.
*/

int32_t pim_erasesector_span( int32_t dev_id, uint32_t sect_no, uint32_t cnt )
{
    FTL_STATUS status;
    unsigned long done;
    switch(dev_id)
    {
    case IONFS_SPAN_DEVA:
        if((status = FTL_DeviceObjectsDelete(sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
            DBG_Printf("ERROR: pim_erasesector_span: sect_no=0x%X, ", sect_no, 0);
            DBG_Printf("cnt=%d\n", cnt, 0);
            return __convert_ionFS_status(status);
        }
        break;
    #if ( IONFS_DEVICE_NUM > 1 ) 
    case IONFS_SPAN_DEVB:
        /*2BDSOL********RENAME TO POINT TO NEW BD:*******/
        if((status = DEV_1_FTL_DeviceObjectsDelete(sect_no, cnt, &done)) != FTL_ERR_PASS)
        {
            DBG_Printf("ERROR: DEV_1 pim_erasesector_span: sect_no=0x%X, ", sect_no, 0);
            DBG_Printf("cnt=%d\n", cnt, 0);
            return __convert_ionFS_status(status);
        }
        break;
    #endif
    default:
        return __convert_ionFS_status(FTL_ERR_FAIL);
    }
    return IONFS_OK;
}

#endif

/*----------------------------------------------------------------------------
 END OF FILE
----------------------------------------------------------------------------*/

