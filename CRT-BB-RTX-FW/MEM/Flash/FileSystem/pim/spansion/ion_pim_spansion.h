/* file: pim_spansion.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


#if !defined( PIM_SPANSION_H )
#define PIM_SPANSION_H
#include "../ion_pim.h"

#define IONFS_SPAN_SECTOR_SIZE 512
#define IONFS_SPAN_SECTOR_SIZE_BITS 9
#define IONFS_SPAN_START_SECTOR 0

#define IONFS_SPAN_DEVA   0    // device A is drive number 0
#if ( IONFS_DEVICE_NUM > 1 ) 
#define IONFS_SPAN_DEVB   1    // device B is drive number 1
#endif
//========= functions =========

int32_t pim_setup_span( pim_devinfo_t *de, char_t * name );
int32_t pim_ioctl_span( int32_t dev_id, pim_ioctl_cmd_t cmd, void *arg );
int32_t pim_readsector_span( int32_t dev_id, uint32_t sect_no, uint8_t *buf, uint32_t cnt );
int32_t pim_writesector_span( int32_t dev_id, uint32_t sect_no, uint8_t *buf, uint32_t cnt );
int32_t pim_erasesector_span( int32_t dev_id, uint32_t sect_no, uint32_t cnt );

#endif

