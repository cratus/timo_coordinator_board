/* FILE: ion_malloc.c */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "../fsm/ion_fsm.h"
#include "ion_malloc.h"


#if defined( IONFS_FS_MALLOC )

/*-----------------------------------------------------------------------------
 DEFINE DEFINITIONS
-----------------------------------------------------------------------------*/

/* Trace checking option */
#define IONFS_TRACE_HEAP      1
#define IONFS_DEBUG_HEAP      0
#define IONFS_BENCHMARK_HEAP  0

#if ( IONFS_BENCHMARK_HEAP == 1 ) && defined( __arm )
   #if (__ARMCC_VERSION > 210000)
      #pragma O3
   #else
      #pragma O2
   #endif
#endif




#define IONFS_USE_FREE_HASH   1
#define IONFS_HASH_BIT        1
#define IONFS_HASH_SIZE       2
#define IONFS_HASH_SCHEME     IONFS_HASH_BIT
#define IONFS_HASH_LOGGING    0


/* The number of list of free blocks */
#if ( IONFS_USE_FREE_HASH == 1 )
#define IONFS_FREE_LIST       23
#endif


#define IONFS_NAME_LEN        12
#define IONFS_STD_LINE  "===================================================" \
                        "============================="


#define IONFS_ALIGNMENT         4ul
#define IONFS_ALIGNMENT_MASK    (IONFS_ALIGNMENT-1ul)
#define IONFS_FREE              0x55U
#define IONFS_ALLOCATED         0xAAU
#define IONFS_GUARD_PATTERN     0xEFBEADDE


#if ( IONFS_USE_FREE_HASH == 1 )
/*
   IONFS_FREE_LIST is:
                    18 : Support max 1MB.
                    19 : Support max 2MB.
                    20 : Support max 4MB.
                    21 : Support max 8MB.
                    22 : Support max 16MB.
                    23 : Support max 32MB.
*/
#define IONFS_BASE_BIT  2  /* the IONFS_MINIMAL_SIZE is 8. */
#endif




/*-----------------------------------------------------------------------------
 DEFINE STRUCTURES
-----------------------------------------------------------------------------*/

/* Include information of heap. */
typedef struct lib_hcb_s {
   char io_name[IONFS_NAME_LEN];  /* name of heap */
   int32_t io_heap_base;  /* base address of heap buffer */
   int32_t io_heap_size;  /* size of the heap buffer */
                          /* NOTE : heap_size must be less than 16MB */
   int32_t io_allocated_blks;  /* allocated number of block */
   int32_t io_available_size;  /* available size */

   #if ( IONFS_USE_FREE_HASH == 1 )
   list_head_t *io_free[IONFS_FREE_LIST];  /* array for list of free blocks */
   uint32_t io_free_mask;  /* mask bit for searching free blocks */
   #else
   list_head_t *io_free_ptr;  /* linked-list for free blocks */
   #endif

   #if ( IONFS_BENCHMARK_HEAP == 1 )
   #if defined( IONFS_USE_FREE_HASH ) && ( IONFS_HASH_LOGGING == 1 )
   int32_t io_max_malloc_hash_cnt[IONFS_FREE_LIST];
   int32_t io_log_malloc_hash_cnt[IONFS_FREE_LIST];
   #endif
   int32_t io_max_allocated_blks;  /* max allocated number of block */
   int32_t io_max_free_blks;  /* min free number of block */
   int32_t io_min_available_size;  /* min available size */
   int32_t io_max_malloc_time;  /* max malloc time */
   int32_t io_max_free_time;  /* max free time */
   int32_t io_total_malloc_cnt;  /* total malloc count. */
   int32_t io_total_free_cnt;  /* total free count. */
   int32_t io_total_malloc_time;  /* total malloc time */
   int32_t io_total_free_time;  /* total free time */
   #endif
} lib_hcb_t;  /* heap control block */


/* Information of all blocks */
typedef struct lib_alloc_head_s {
   uint8_t hd_state;

   uint8_t hd_pad[3];

   list_head_t hd_lst;

   #if ( IONFS_BASE_BIT == 2 )
   uint32_t hd_pad2;
   #endif

} lib_alloc_head_t;


/* Information of all free blocks */
typedef struct lib_free_head_s {
   uint8_t hd_state;

   uint8_t hd_pad[3];

   list_head_t hd_lst;

   list_head_t hd_free;

} lib_free_head_t;




/* The minimum size of a block */
#define IONFS_MINIMAL_SIZE   (sizeof(lib_free_head_t)-sizeof(lib_alloc_head_t))
/* The size of a block's head */
#define IONFS_HEAD_SIZE      (sizeof(lib_alloc_head_t))
/* The minimum size of a free block */
#define IONFS_MIN_FREE_SIZE  (sizeof(lib_free_head_t)+12)




#if ( __ARMCC_VERSION > 210000 )
#define lib_cpu_lr()  __return_address()
#else
#define lib_cpu_lr()  0
#endif
#define lib_disable_interrupts()  0
#define lib_restore_cpustate(cpu)  (UNUSED(cpu))

#if ( IONFS_USE_FREE_HASH == 1 )
/* Get hash through a block's size */
#define lib_get_free_hash(size)  ((int32_t)((int32_t)bit_flo(size)-\
                                   (int32_t)IONFS_BASE_BIT))
/* Check whether av free block exists in a free block list indicated by a
   specific hash or not */
#define lib_is_available(hcb, bit)  ((hcb)->io_free_mask & bit_val(bit))
#endif
#define lib_get_data_ptr(head)  ((void*)((uint8_t*)(head) + IONFS_HEAD_SIZE))
#define lib_get_head_ptr(block)  ((lib_free_head_t*) ((uint8_t*)(block) - \
                                  IONFS_HEAD_SIZE))
#define lib_get_free_size(free) \
   ((uint8_t*) list_entry((free)->hd_lst.next, lib_free_head_t, hd_lst) - \
    ((uint8_t*)(free)+IONFS_HEAD_SIZE))


/* Target dependent configuration */
#define lib_set_return_address(head) (head->hd_pad[0]=lib_cpu_lr(),\
                                      head->hd_pad[1]=lib_cpu_lr()>>8,\
                                      head->hd_pad[2]=lib_cpu_lr()>>16)
#define lib_get_return_address(head) (uint32_t) (head->hd_pad[0]|\
                                      (head->hd_pad[1]<<8)|\
                                      (head->hd_pad[2]<<16)|\
                                      ((uint32_t)&lib_dump_malloc\
                                      & 0xff000000))
#if ( IONFS_BENCHMARK_HEAP == 1 )
#define lib_get_time() ((uint16_t)(0))
#define lib_elapsed_time(before,after) (((after) > (before)) ? \
                               (0xFFFF-(after)+(before)) : ((before)-(after)) )
#endif




/*-----------------------------------------------------------------------------
 DEFINE ENUMERATIONS
-----------------------------------------------------------------------------*/

/* Define a enumeration for exception related to memory allocation */
typedef enum {
   HEAP_OK = 0,
   HEAP_CREATE_FAIL = -1,
   HEAP_ALLOC_CORRUPT = -2,
   HEAP_FREE_NULL = -3,
   HEAP_FREE_FREE = -4,
   HEAP_FREE_CORRUPT = -5,
   HEAP_FREE_OUT_OF_MEMORY = -6,
   HEAP_CHECK_CORRUPT = -7,
   HEAP_OUT_OF_MEMORY = -8,
   HEAP_TOO_MANY_FRAGMENTATION = -9,
   HEAP_ALLOC_NULL = -10

} lib_exceptlib_heap_t;




/*-----------------------------------------------------------------------------
 DEFINE FUNCTIONS
-----------------------------------------------------------------------------*/

#if ( IONFS_HASH_LOGGING == 1 )

/*
 Name: __lib_add_hash_log
 Desc: Increase the count of free blocks at free block list by hash got
       through size.
 Params:
   - hcb: Pointer to heap control block.
   - size: A size of free block.
 Returns: None.
 Caveats: None.
*/

static void __lib_add_hash_log( lib_hcb_t *hcb, int32_t size )
{
   int32_t free_hash = lib_get_free_hash(size);


   hcb->io_log_malloc_hash_cnt[free_hash]++;
   if ( hcb->io_log_malloc_hash_cnt[free_hash] > hcb->io_max_malloc_hash_cnt[free_hash] )
      hcb->io_max_malloc_hash_cnt[free_hash] = hcb->io_log_malloc_hash_cnt[free_hash];
}




/*
 Name: __lib_del_hash_log
 Desc: Decrease the count of free blocks at free block list by hash got
       through size.
 Params:
   - hcb: Pointer to heap control block.
   - size: A size of free block.
 Returns: None.
 Caveats: None.
*/

static void __lib_del_hash_log( lib_hcb_t *hcb, int32_t size )
{
   int32_t free_hash = lib_get_free_hash(size);


   hcb->io_log_malloc_hash_cnt[free_hash]--;
}
#else
#define __lib_add_hash_log(hcb, size)
#define __lib_del_hash_log(hcb, size)
#endif




/*
 name:  __lib_exceptlib_handling
 Desc: Handle exceptions about memory allocation.
 Params:
   - hcb: Pointer to heap control block.
   - expt_type: A type of exception.
 Returns: None.
 Caveats: None.
*/

static void __lib_exceptlib_handling( lib_hcb_t *hcb, lib_exceptlib_heap_t expt_type )
{
   switch ( expt_type ) {
      case HEAP_CREATE_FAIL:
      case HEAP_ALLOC_CORRUPT:
      case HEAP_FREE_FREE:
      case HEAP_FREE_CORRUPT:
      case HEAP_FREE_NULL:
      case HEAP_ALLOC_NULL:
         ionFS_exit( expt_type );
      case HEAP_OUT_OF_MEMORY:
      case HEAP_TOO_MANY_FRAGMENTATION:
         break;
   }
}




/*
 name:  __lib_init_free_ptr
 Desc: Initialize free blocks in a specific heap control block.
 Params:
   - hcb: Pointer to heap control block.
   - free: Information of all free blocks pointer.
 Returns: None.
 Caveats: None.
*/

static void __lib_init_free_ptr( lib_hcb_t *hcb, lib_free_head_t *free )
{
   #if ( IONFS_USE_FREE_HASH == 1 )
   int32_t free_hash,
           i;


   /* Initialize free list. */
   for ( i = 0; i < IONFS_FREE_LIST; i++ ) {
      hcb->io_free[i] = NULL;
      #if defined( IONFS_USE_FREE_HASH ) && ( IONFS_HASH_LOGGING == 1 )
      hcb->io_max_malloc_hash_cnt[i] = 0;
      hcb->io_log_malloc_hash_cnt[i] = 0;
      #endif
   }

   /* Set free block. */
   free_hash = lib_get_free_hash(hcb->io_available_size);
   hcb->io_free[free_hash] = &free->hd_free;
   hcb->io_free_mask = bit_val(free_hash);
   #else
   hcb->io_free_ptr = &free->hd_free;
   #endif

   __lib_add_hash_log( hcb, lib_get_free_size(free) );
}




/*
 Name: lib_creat_malloc
 Desc: Create a Heap.
 Params:
   - hcb: Pointer to the heap control block.
   - name: A pointer to the null-terminated name of heap to be created.
   - heap_base: A base address of heap to be created.
   - heap_size: A size of heap to be create.
 Returns: None.
 Caveats: None.
*/

int32_t lib_creat_malloc( mcb_t *mcb, char *name, void *heap_base, int32_t heap_size )
{
   lib_hcb_t *hcb = (lib_hcb_t *) mcb;
   lib_alloc_head_t *alloc;
   lib_free_head_t *free;


   if ( sizeof(mcb_t) < sizeof(lib_hcb_t) )
      return os_set_errno(IONFS_EPORT);

   lib_strncpy( hcb->io_name, name, IONFS_NAME_LEN );

   /* Initialize heap-area infomations. */
   hcb->io_heap_base = (uint32_t)heap_base;
   hcb->io_allocated_blks = 0;

   /* Allocate the reserved block that prevents wrap-around-merging. */
   alloc = (lib_alloc_head_t*) (hcb->io_heap_base + heap_size - IONFS_HEAD_SIZE);
   alloc->hd_state = IONFS_ALLOCATED;
   list_init( &alloc->hd_lst );

   hcb->io_available_size = heap_size - IONFS_HEAD_SIZE/*The reserved block's header*/;

   /* Initialize the first free-block. */
   free = (lib_free_head_t*)hcb->io_heap_base;
   free->hd_state = IONFS_FREE;
   list_init( &free->hd_lst );
   list_init( &free->hd_free );  /* Initialize free entry. */

   /* Link free block with allocated block. */
   list_add_tail( &free->hd_lst, &alloc->hd_lst );

   hcb->io_available_size -= IONFS_HEAD_SIZE/*Free block*/;
   hcb->io_heap_size = hcb->io_available_size;

   __lib_init_free_ptr( hcb, free );

   #if ( IONFS_BENCHMARK_HEAP == 1 )
   hcb->io_max_allocated_blks = 0;
   hcb->io_max_free_blks = 0;
   hcb->io_min_available_size = hcb->io_available_size;
   hcb->io_max_malloc_time = 0;
   hcb->io_max_free_time = 0;
   hcb->io_total_malloc_cnt = 0;
   hcb->io_total_free_cnt = 0;
   hcb->io_total_malloc_time = 0;
   hcb->io_total_free_time = 0;
   #endif

   return IONFS_OK;
}




/*
 Name: lib_delete_malloc
 Desc: Reserved.
 Params:
   - hcb: Pointer to the heap control block.
 Returns: None.
 Caveats: None.
*/

void lib_delete_malloc( mcb_t *mcb )
{
}




/*
 Name: __lib_add_free_list
 Desc: Add a block to the free block list.
 Params:
   - hcb: Pointer to the heap control block.
   - head: header of the block to be added.
 Returns: None.
 Caveats: None.
*/

static void __lib_add_free_list( lib_hcb_t *hcb, lib_free_head_t *head )
{
   list_head_t **p_free_ptr;
   #if ( IONFS_USE_FREE_HASH == 1 )
   int32_t free_hash;
   int32_t free_size;


   free_size = lib_get_free_size(head);
   free_hash = lib_get_free_hash(free_size);
   p_free_ptr = &hcb->io_free[free_hash];
   #else
   p_free_ptr = &hcb->io_free_ptr;
   #endif

   if ( NULL == *p_free_ptr ) {
      list_init( &head->hd_free );
      /* Register to the free-map. */
      *p_free_ptr = &head->hd_free;
      #if ( IONFS_USE_FREE_HASH == 1 )
      bit_set( &hcb->io_free_mask, free_hash );
      #endif
   }
   else
      /* Add to the free-list. */
      list_add( *p_free_ptr, &head->hd_free );

   __lib_add_hash_log( hcb, free_size );

   head->hd_state = IONFS_FREE;
}




/*
 Name: __lib_del_free_ptr
 Desc: Delete a block from the free map used hash.
 Params:
   - hcb: Pointer to the heap control block.
   - head: header of the block to be deleted.
 Returns:
   int32_t  > 0 on success. The return value is the size of deleted block.
            <=0 on fail.
 Caveats: None.
*/

static int32_t __lib_del_free_ptr( lib_hcb_t *hcb, lib_free_head_t *head )
{
   int32_t free_size = lib_get_free_size(head);
   list_head_t **p_free_ptr;


   #if ( IONFS_USE_FREE_HASH == 1 )
   int32_t free_hash;
   free_hash = lib_get_free_hash(free_size);
   p_free_ptr = &hcb->io_free[free_hash];
   #else
   p_free_ptr = &hcb->io_free_ptr;
   #endif

   /* Delete from the free-map. */
   if ( *p_free_ptr == &head->hd_free ) {
      if ( head->hd_free.prev == &head->hd_free ) {
         *p_free_ptr = NULL;
         #if ( IONFS_USE_FREE_HASH == 1 )
         bit_clear( &hcb->io_free_mask, free_hash );
         #endif
      }
      else
         *p_free_ptr = head->hd_free.prev;
   }

   return free_size;
}




/*
 Name: __lib_del_free_list
 Desc: Delete a block from the free block list.
 Params:
   - hcb: Pointer to the heap control block.
   - head: header of the block to be deleted.
 Returns:
   int32_t  > 0 on success. The return value is the size of deleted block.
            <=0 on fail.
 Caveats: None.
*/

static int32_t __lib_del_free_list( lib_hcb_t *hcb,
                                     lib_free_head_t *head )
{
   int32_t free_size;


   free_size = __lib_del_free_ptr( hcb, head );

   /* Delete from the free-list. */
   list_del_just( &head->hd_free );

   __lib_del_hash_log( hcb, free_size );

   return free_size;
}




#if ( IONFS_USE_FREE_HASH == 1 )
/*
 Name: __lib_get_next_free_hash
 Desc: Get hash about free block list of larger size than block's size in free
       block list indicated by a current hash.
 Params:
   - hcb: Pointer to the heap control block.
   - curr_free_bit: The current hash value.
 Returns:
   int32_t  value on success. The return value is a new hash value.
 Caveats: If free block in free block list indicated hash is empty, this
          function is called.
*/

static int32_t __lib_get_next_free_hash( lib_hcb_t *hcb,
                                         int32_t curr_free_bit )
{
   int32_t free_bits,
           free_hash = curr_free_bit;


   free_bits = hcb->io_free_mask;

   free_bits >>= free_hash + 1/*free_hash is started from 0.*/;

   if ( 0 == free_bits )
      return -1;

   free_hash = (bit_ffo(free_bits) + 1/*bit_ffo() return from 0 to 31*/) + free_hash;

   fsm_assert2( NULL != hcb->io_free[free_hash] );

   return free_hash;
}
#endif




/*
 Name: __lib_lookup_free_list
 Desc: Search a list of free blocks and return a pointer to the head of list.
 Params:
   - hcb: Pointer to the heap control block.
   - size: A size of block to be searched.
 Returns:
   lib_free_head_t*  value on success. The return value is a pointer to the
                                         head of the list.
                       NULL on fail.
 Caveats: None.
*/

static lib_free_head_t *__lib_lookup_free_list( lib_hcb_t *hcb,
                                                   int32_t size )
{
   lib_free_head_t *first_head,
                    *head;
   int32_t free_size;
   #if ( IONFS_USE_FREE_HASH == 1 )
   int32_t free_hash;


   /* Get hash through block's size. */
   free_hash = lib_get_free_hash(size);

   if ( false == lib_is_available( hcb, free_hash ) )
      goto search_next_free;

   /* Get the head from free block list. */
   first_head = head = list_entry( hcb->io_free[free_hash], lib_free_head_t,
                                   hd_free );
   #else
   first_head = head = list_entry(hcb->io_free_ptr, lib_free_head_t, hd_free);
   #endif

   if ( NULL == head ) return NULL;

   do {
      free_size = lib_get_free_size(head);
      if ( free_size >= size )
         return head;

      /* Search for the left in the free-list. */
      head = list_entry( head->hd_free.prev, lib_free_head_t, hd_free );
   } while ( head != first_head );

#if ( IONFS_USE_FREE_HASH == 1 )
search_next_free:

   free_hash = __lib_get_next_free_hash( hcb, free_hash );
   if ( 0 < free_hash ) {
      head = list_entry(hcb->io_free[free_hash], lib_free_head_t, hd_free);
      fsm_assert2( NULL != head );
      return head;
   }
#endif

   return NULL;
}




/*
 Name: lib_malloc
 Desc: Allocate a block in a heap.
 Params:
   - hcb: Pointer to the heap control block.
   - size: A size of block to be allocated.
 Returns:
   void*  value on success. The return value is a address of allocated block.
          NULL on fail.
 Caveats: None.
*/

void* lib_malloc( mcb_t *mcb, int32_t size )
{
   lib_hcb_t *hcb = (lib_hcb_t *) mcb;
   lib_free_head_t *head,
                   *next;
   void *alloc_addr = NULL;
   int32_t no_align,
           free_size,
           cpu_state;
   #if ( IONFS_BENCHMARK_HEAP == 1 )
   int32_t timetick;
   #endif


   fsm_assert1( 0 != size );

   #if ( IONFS_BENCHMARK_HEAP == 1 )
   timetick = lib_get_time();
   #endif

   /* Initialize local variables. */
   free_size = 0;

   /* Adjust alignment. */
   no_align = size & IONFS_ALIGNMENT_MASK;
   if ( no_align )
      size += IONFS_ALIGNMENT - no_align;

   size = (size > IONFS_MINIMAL_SIZE) ? size : IONFS_MINIMAL_SIZE;

   if ( (int32_t )(size+IONFS_HEAD_SIZE) > hcb->io_heap_size ) {
      __lib_exceptlib_handling( hcb, HEAP_OUT_OF_MEMORY );
      return NULL;
   }

   cpu_state = lib_disable_interrupts();

   if ( hcb->io_available_size >= size ) {
      head = __lib_lookup_free_list( hcb, size );
      if ( head ) {
         free_size = __lib_del_free_ptr( hcb, head );
         __lib_del_hash_log( hcb, lib_get_free_size(head) );

         /* If the block needs to be split. */
         if ( free_size >= (size + (int32_t)IONFS_MIN_FREE_SIZE) ) {
            /* Split the new head-block. */
            next = (lib_free_head_t*) ((uint8_t *)head+IONFS_HEAD_SIZE + size);
            /* Add the block-list. */
            list_add( &head->hd_lst, &next->hd_lst );

            __lib_add_free_list( hcb, next );

            hcb->io_available_size -= (size+IONFS_HEAD_SIZE/*Split block's header.*/);
         }
         else
            hcb->io_available_size -= free_size;

         /* Extract the alloc-block. (Link the each neighbor) */
         list_del_just( &head->hd_free );

         head->hd_state = IONFS_ALLOCATED;

         /* calculate address that will allocate */
         alloc_addr = lib_get_data_ptr(head);

         /* increase number of allocated block */
         hcb->io_allocated_blks++;


         #if ( IONFS_TRACE_HEAP == 1 )
         lib_set_return_address(head);
         #endif

         #if ( IONFS_DEBUG_HEAP == 1 )
         *(uint32_t*)alloc_addr = IONFS_GUARD_PATTERN;
         #endif

         #if ( IONFS_BENCHMARK_HEAP == 1 )
         if ( hcb->io_allocated_blks > hcb->io_max_allocated_blks )
            hcb->io_max_allocated_blks = hcb->io_allocated_blks;
         if ( hcb->io_available_size < hcb->io_min_available_size )
            hcb->io_min_available_size = hcb->io_available_size;
         hcb->io_total_malloc_cnt++;
         #endif
      }
   }

   lib_restore_cpustate( cpu_state );

   return alloc_addr;
}




/*
 Name: lib_free
 Desc: Free a block in a heap.
 Params:
   - hcb: Pointer to the heap control block.
   - block: Address of the block to be free.
 Returns: None.
 Caveats: None.
*/

void lib_free( mcb_t *mcb, void *block )
{
   lib_hcb_t *hcb = (lib_hcb_t *) mcb;
   lib_free_head_t *head,
                   *next,
                   *prev;
   uint32_t free_size,
            cpu_state;
   #if ( IONFS_BENCHMARK_HEAP == 1 )
   int32_t timetick;
   #endif


   #if ( IONFS_BENCHMARK_HEAP == 1 )
   timetick = lib_get_time();
   #endif

   /* Check out of heap-area */
   if ( ( (int32_t)block < hcb->io_heap_base ) ||
        ( (int32_t)block > (hcb->io_heap_base + hcb->io_heap_size) ) ) {
      __lib_exceptlib_handling( hcb, HEAP_FREE_OUT_OF_MEMORY );
      return;
   }

   /* check null-pointer */
   if ( NULL == block ) {
      __lib_exceptlib_handling( hcb, HEAP_FREE_NULL );
      return;
   }

   /* Calculate head address */
   head = lib_get_head_ptr(block);

   /* check for free-free */
   if ( IONFS_FREE == head->hd_state ) {
      __lib_exceptlib_handling( hcb, HEAP_FREE_FREE );
      return;
   }

   /* Check for corruption */
   if ( IONFS_ALLOCATED != head->hd_state ) {
      __lib_exceptlib_handling( hcb, HEAP_FREE_CORRUPT );
      return;
   }

   cpu_state = lib_disable_interrupts();

   /* Save the size of free-block */
   free_size = lib_get_free_size(head);

   /* decrease number of allocated block*/
   hcb->io_allocated_blks--;

   /* decrease available_size*/
   hcb->io_available_size += free_size;

   prev = list_entry(head->hd_lst.prev, lib_free_head_t, hd_lst);
   next = list_entry(head->hd_lst.next, lib_free_head_t, hd_lst);

   if ( IONFS_FREE == prev->hd_state ) {
      /* Delete from the free-list. */
      free_size += __lib_del_free_list( hcb, prev ) + IONFS_HEAD_SIZE;
      hcb->io_available_size += IONFS_HEAD_SIZE;

      /* Delete from the hd_lst. */
      list_del_just( &head->hd_lst );

      /* Remapping head to the prev-head. */
      head = prev;
   }

   /* Merge head with the next. */
   if ( IONFS_FREE == next->hd_state ) {
      /* Delete from the free-list. */
      free_size += __lib_del_free_list( hcb, next ) + IONFS_HEAD_SIZE;
      hcb->io_available_size += IONFS_HEAD_SIZE;

      /* Delete from the hd_lst. */
      list_del_just( &next->hd_lst );
   }

   /* Add to the free-list. */
   __lib_add_free_list( hcb, head );


   #if ( IONFS_BENCHMARK_HEAP == 1 )
   timetick = lib_elapsed_time(timetick);
   if ( timetick > hcb->io_max_free_time )
      hcb->io_max_free_time = timetick;
   hcb->io_total_free_cnt++;
   hcb->io_total_free_time += timetick;
   #endif

   lib_restore_cpustate( cpu_state );
}




/*
 Name: lib_delete
 Desc: Delete a block in a heap.
 Params:
   - hcb: Pointer to the heap control block.
   - block: Address of the block to be deleted.
 Returns: None.
 Caveats: None.
*/

void lib_delete( mcb_t *mcb, void *block )
{
   if ( block )
      lib_free( mcb, block );
}




/*
 Name: lib_clean
 Desc: Delete a block in a heap.
 Params:
   - hcb: Pointer to the heap control block.
   - p_block: Pointer to the block to be deleted.
 Returns: None.
 Caveats: Pointer to the block initialized to NULL.
*/

void lib_clean( mcb_t *mcb, void **p_block )
{
   lib_delete( mcb, *p_block );
   *p_block = NULL;
}




/*
 Name: lib_check_malloc
 Desc: Check status of the memory allocation in a heap.
 Params:
   - hcb: Pointer to the heap control block.
 Returns:
   bool_t  true always.
 Caveats: If heap is corrupted, a exception ocurrs.
*/

bool_t lib_check_malloc( mcb_t *mcb )
{
   lib_hcb_t *hcb = (lib_hcb_t *) mcb;
   lib_free_head_t *head;
   uint32_t heap_end;
   uint32_t allocated_blks = 0;


   head = (lib_free_head_t*) hcb->io_heap_base;
   heap_end = hcb->io_heap_base + hcb->io_heap_size;

   do {
      if ( IONFS_ALLOCATED == head->hd_state )
         allocated_blks++;
      else if ( IONFS_FREE != head->hd_state )
         __lib_exceptlib_handling( hcb, HEAP_CHECK_CORRUPT );

      head = list_entry(head->hd_lst.next, lib_free_head_t, hd_lst);
   } while ( (uint32_t)head < heap_end );

   if ( allocated_blks != hcb->io_allocated_blks )
      __lib_exceptlib_handling( hcb, HEAP_CHECK_CORRUPT );

   return true;
}




/*
 Name: lib_dump_malloc
 Desc: Dump status of the memory allocation in a heap.
 Params:
   - hcb: Pointer to the heap control block.
   - cprint: Pointer to function which dump data is printed.
   - cs: Pointer to console which dump data is printed.
 Returns: None.
 Caveats: None.
*/

void lib_dump_malloc( mcb_t *mcb, cprint_t cprint, console_t cs )
{
   lib_hcb_t *hcb = (lib_hcb_t *) mcb;
   lib_free_head_t *head;
   uint32_t heap_end,
            alloc_blks = 0,
            free_blks = 0,
            max_freeblock_size = 0,
            size;


   cprint( cs, "\r\n" "lnk dump malloc\r\n" );
   cprint( cs, "\r\n" IONFS_STD_LINE "\r\n" );

   cprint( cs, "name: %s\r\n", hcb->io_name );
   cprint( cs, "heap base: 0x%08X\r\n", hcb->io_heap_base );
   cprint( cs, "heap size: 0x%08X(%u)\n", hcb->io_heap_size, hcb->io_heap_size );
   cprint( cs, "allocated blocks: %u\r\n", hcb->io_allocated_blks );
   cprint( cs, "available size: 0x%08X(%u)\n",
                  hcb->io_available_size, hcb->io_available_size );
   #if ( IONFS_BENCHMARK_HEAP == 1 )
   cprint( cs, "max malloc time: %u\r\n", hcb->io_max_malloc_time );
   cprint( cs, "max free time: %u\r\n", hcb->io_max_free_time );
   cprint( cs, "max allocated blocks: %u\r\n", hcb->io_max_allocated_blks );
   cprint( cs, "max free blocks: %u\r\n", hcb->io_max_free_blks );
   cprint( cs, "min available size: 0x%08X(%u)\r\n",
                  hcb->io_min_available_size, hcb->io_min_available_size );
   cprint( cs, "total malloc cnt: %u\r\n", hcb->io_total_malloc_cnt );
   cprint( cs, "total free cnt: %u\r\n", hcb->io_total_free_cnt );
   cprint( cs, "total malloc time: %u\r\n", hcb->io_total_malloc_time );
   cprint( cs, "total free time: %u\r\n", hcb->io_total_malloc_time );
   #if defined( IONFS_USE_FREE_HASH ) && ( IONFS_HASH_LOGGING == 1 )
   {
      int32_t i;
      for ( i = 0; i < IONFS_FREE_LIST; i++ )
         cprint( cs, "max malloc cnt hash[%02u]: %u\r\n", i,
                hcb->io_max_malloc_hash_cnt[i] );
   }
   #endif
   #endif


   head = (lib_free_head_t*)hcb->io_heap_base;
   heap_end = hcb->io_heap_base + hcb->io_heap_size;

   cprint( cs, "\r\n----------------- dump blocks ------------------\r\n" );
   cprint( cs, "status  index   address       size                [return " \
               "address]\r\n" );
   do {
      size = lib_get_free_size(head);
      if ( IONFS_ALLOCATED == head->hd_state ) {
         cprint( cs, "[A]     %04u    0x%08X    0x%X(%u)", alloc_blks++, head,
                size, size );
         #if ( IONFS_TRACE_HEAP == 1 )
         cprint( cs, "    0x%08X", lib_get_return_address(head) );
         #endif
         #if ( IONFS_DEBUG_HEAP == 1 )
         if ( IONFS_GUARD_PATTERN == *(uint32_t*)(head+1) ) cprint( cs,
              "(not used)" );
         #endif
         cs( '\r' );
         cs( '\n' );
      }
      else if ( IONFS_FREE == head->hd_state ) {
         if ( size > max_freeblock_size )
            max_freeblock_size = size;

         cprint( cs, "[F]     %04u    0x%08X    0x%X(%u)", free_blks++, head,
                size, size );
      }
      else
         cprint( cs, "\t\t\b panic \b [0x%08X] is corrupted!\r\n", head );

      head = list_entry(head->hd_lst.next, lib_free_head_t, hd_lst);
   } while ( (uint32_t)head < heap_end );

   cprint( cs, "\r\n" IONFS_STD_LINE "\r\n" );

   cprint( cs, "\r\nmax free block size: 0x%08X(%u)\r\n",
                                    max_freeblock_size, max_freeblock_size );

   cprint( cs, "\r\n" IONFS_STD_LINE "\r\n" );
}

#endif /*IONFS_FS_MALLOC*/
/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/

