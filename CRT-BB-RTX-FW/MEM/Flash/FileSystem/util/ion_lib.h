/* FILE: ion_lib.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


#if !defined( IONFS_LIB_H_26112005 )
#define IONFS_LIB_H_26112005

/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "../osd/ion_osd.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#if defined( IONFS_UNICODE )
#include <wchar.h>
#endif




#if defined (__cplusplus )
extern "C" {
#endif




/*-----------------------------------------------------------------------------
 DECLARE FUNCTION PROTO-TYPE
-----------------------------------------------------------------------------*/

uint32_t lib_rand( void );
void lib_srand( uint32_t seed );
void lib_memcpy( void *dst, const void *src, uint32_t len );
void lib_memcpy8_16( uint8_t *dst, uint16_t *src, uint32_t len16 );
void lib_memcpy16( uint16_t *dst, uint16_t *src, uint32_t len16 );
void lib_memset( void *dst, uint8_t byte, uint32_t len );
void lib_memset16( void *dst, uint16_t word, uint32_t len16 );
void lib_memset32( void *dst, uint32_t dword, uint32_t len32 );
uint32_t lib_memcmp( const void *s1, const void *s2, uint32_t len );
uint32_t lib_data32cmp( const uint32_t *p, uint32_t data, uint32_t len32 );
uint32_t lib_strcat( char *dst, const char *src );
uint32_t lib_strcmp( const char *s1, const char *s2 );
uint32_t lib_stricmp( const char *s1, const char *s2 );
uint32_t lib_strlen( const char *str );
int32_t lib_lstrcpy( char *dst, const char *src );
int32_t lib_lstrncpy( char *dst, const char *src, int32_t max_len );
char *lib_strncpy( char *dst, const char *src, uint32_t len );
const void *lib_strchr( const char *src, char ch );
const void *lib_strrchr( const char *src, char ch );
const void *lib_strnchr( const char *src, const char ch, int n );
uint32_t lib_wcscat( uint16_t *dst, const uint16_t *src );
uint32_t lib_wcscmp( const uint16_t *s1, const uint16_t *s2 );
uint32_t lib_wcsicmp( const uint16_t *s1, const uint16_t *s2 );
uint32_t lib_wcslen( const uint16_t *str );
int32_t lib_lwcscpy( uint16_t *dst, const uint16_t *src );
int32_t lib_lwcsncpy( uint16_t *dst, const uint16_t *src, int32_t len );
const void *lib_wcschr( const uint16_t *src, uint16_t ch );
const void *lib_wcsnchr( const uint16_t *src, const uint16_t ch, int n );
int32_t lib_lwcscpy( uint16_t *dst, const uint16_t *src );
uint32_t lib_log2( uint32_t val );
uint32_t lib_ucs2_2_utf8( const uint16_t ucs2, uint8_t *utf8 );




/*-----------------------------------------------------------------------------
 DEFINITIONS FOR FUNCTIONS
-----------------------------------------------------------------------------*/

/* Library Macro */
#define lib_cdiv(a,b)      (((a)+((b)-1))/(b))
#define lib_sh_fdiv(a,b)   ((a)>>(b)) /* floor div */
#define lib_sh_cdiv(a,b)   (((a)+(1<<(b))-1)>>(b)) /* ceiling div */
#define lib_tolower(c)     ((((c)>='A')&&((c)<='Z')) ? (c+0x20) : (c))
#define lib_toupper(c)     ((((c)>='a')&&((c)<='z')) ? (c-0x20) : (c))
#define lib_isupper(c)     ((((c)>='A')&&((c)<='Z')) ? true : false)
#define lib_islower(c)     ((((c)>='a')&&((c)<='z')) ? true : false)
#define lib_isdigit(c)     ((((c)>='0')&&((c)<='9')) ? true : false)


#if defined (USE_STD_LIB)
#define ionFS_memcpy(dst, src, len)    memcpy(dst, src, len)
#define ionFS_strcpy(dst, src)         strcpy(dst, src)
#define ionFS_strcat(dst, src)         strcat(dst, src)
#define ionFS_strlen(s)                strlen(s)
#define ionFS_memset(dst, byte, len)   memset(dst, byte, len)
#define ionFS_strncpy(dst, src, len)   strncpy(dst, src, len)
#define ionFS_memcmp(s1, s2, len)      memcmp(s1, s2, len)
#else
#define ionFS_memcpy(dst, src, len)    lib_memcpy(dst, src, len)
#define ionFS_strcpy(dst, src)         lib_lstrcpy(dst, src)
#define ionFS_strcat(dst, src)         lib_strcat(dst, src)
#define ionFS_strlen(s)                lib_strlen(s)
#define ionFS_memset(dst, byte, len)   lib_memset(dst, byte, len)
#define ionFS_strncpy(dst, src, len)   lib_strncpy(dst, src, len)
#define ionFS_memcmp(s1, s2, len)      lib_memcmp(s1, s2, len)
#endif


/* Use in ionFS */
#if defined( WIN32 )
#define ionFS_vsprintf                 vsprintf
#define ionFS_sprintf                  sprintf
#else
#define ionFS_vsprintf                 vsprintf
#define ionFS_sprintf                  sprintf
#endif
#define ionFS_time()                   FSTimeStamp()
#define ionFS_localtime()              os_localtime()

#define ionFS_srand(sd)                lib_srand(sd)
#define ionFS_rand()                   lib_rand()

#define ionFS_memcpy8(dst, src, len)      lib_memcpy(dst, src, len)
#define ionFS_memcpy16(dst, src, len16)   lib_memcpy16(dst, src, len16)

#define ionFS_memset16(dst, word, len16)  lib_memset16(dst, word, len16)
#define ionFS_memset32(dst, dword, len32) lib_memset32(dst, dword, len32)

#define ionFS_data32cmp(p, data, len32)   lib_data32cmp(p, data, len32)
#define ionFS_cdiv(a,b)                   lib_cdiv(a,b)
#define ionFS_sh_fdiv(a,b)                lib_sh_fdiv(a,b)
#define ionFS_sh_cdiv(a,b)                lib_sh_cdiv(a,b)
#define ionFS_tolower(c)                  lib_tolower(c)
#define ionFS_toupper(c)                  lib_toupper(c)
#define ionFS_isupper(c)                  lib_isupper(c)
#define ionFS_islower(c)                  lib_islower(c)
#define ionFS_isdigit(c)                  lib_isdigit(c)
#if defined( __arm ) && (__ARMCC_VERSION > 210000)  && !(defined(__TARGET_CPU_ARM920T) || defined(__TARGET_CPU_ARM7TDMI))
#define ionFS_break()                     __breakpoint(0xFE)
#elif defined( WIN32 )
#define ionFS_break()                     __debugbreak()
#else
#define ionFS_break()                     os_break()
#endif
#define ionFS_exit(code)                  os_exit(code)


/* Type-conversion Macro */
#define ARR8_2_UINT16(pU8) ((uint16_t)((pU8)[0] | ((pU8)[1]<<8)))
#define ARR8_2_UINT32(pU8) ((uint32_t)((pU8)[0] | ((pU8)[1]<<8) | \
                            ((pU8)[2]<<16) | ((pU8)[3]<<24)))
#define UINT16_2_ARR8(pU8, U16) do {\
                            ((uint8_t*)(pU8))[0]=(uint8_t)(U16);\
                            ((uint8_t*)(pU8))[1]=(uint8_t)((uint32_t)(U16)>>8);\
                            } while ( 0 )
#define UINT32_2_ARR8(pU8, U32) do {\
                            ((uint8_t*)(pU8))[0]=(uint8_t)(U32);\
                            ((uint8_t*)(pU8))[1]=(uint8_t)((uint32_t)(U32)>>8);\
                            ((uint8_t*)(pU8))[2]=(uint8_t)((uint32_t)(U32)>>16);\
                            ((uint8_t*)(pU8))[3]=(uint8_t)((uint32_t)(U32)>>24);\
                            } while ( 0 )


#if defined( IONFS_UNICODE )

#if defined (USE_STD_LIB)
#define ionFS_t_strcpy(dst, src)    wcscpy((uint16_t*)dst,(const uint16_t*)src)
#define ionFS_t_strcat(dst, src)    wcscat((uint16_t*)dst,(const uint16_t*)src)
#define ionFS_t_strcmp(s1, s2)      wcscmp((const uint16_t*)s1,(const uint16_t*)s2)
#define ionFS_t_strlen(s)           wcslen((const uint16_t*)s)
#define ionFS_t_strchr(str, c)      wcschr((const uint16_t*)str, (uint16_t)c)
#else
#define ionFS_t_strcpy(dst, src)    lib_lwcscpy((uint16_t*)dst,(const uint16_t*)src)
#define ionFS_t_strcat(dst, src)    lib_wcscat((uint16_t*)dst,(const uint16_t*)src)
#define ionFS_t_strcmp(s1, s2)      lib_wcscmp((const uint16_t*)s1,(const uint16_t*)s2)
#define ionFS_t_strlen(s)           lib_wcslen((const uint16_t*)s)
#define ionFS_t_strchr(str, c)      lib_wcschr((const uint16_t*)str, (uint16_t)c)
#endif

#define ionFS_t_lstrcpy(dst, src)         lib_lwcscpy((uint16_t*)dst,(const uint16_t*)src)
#define ionFS_t_lstrncpy(dst, src, len)   lib_lwcsncpy((uint16_t*)dst,(const uint16_t*)src, len)
#define ionFS_t_stricmp(s1, s2)           lib_wcsicmp((const uint16_t*)s1,(const uint16_t*)s2)
#define ionFS_t_strnchr(str, c, n)        lib_wcsnchr((const uint16_t*)str,(uint16_t)c, (int)(n))
#define ionFS_t_toupper(c)                ionFS_toupper(c)
#define ionFS_t_islower(c)                ionFS_islower(c)
#define ionFS_t_isupper(c)                ionFS_isupper(c)
#define ionFS_t_isdigit(c)                ionFS_isdigit(c)

#else

#if defined (USE_STD_LIB)
#define ionFS_t_strcpy(dst, src)          strcpy((char*)dst,(const char*)src)
#define ionFS_t_strcat(dst, src)          strcat((char*)dst,(const char*)src)
#define ionFS_t_strcmp(s1, s2)            strcmp((char*)s1,(char*)s2)
#define ionFS_t_strlen(s)                 strlen((const char *)s)
#define ionFS_t_strchr(str, c)            strchr((const char *)(str),(const char)(c))
#define ionFS_t_strrchr(str, c)           strrchr((const char *)(str),(const char)(c))
#else
#define ionFS_t_strcpy(dst, src)          lib_lstrcpy((char*)dst,(const char*)src)
#define ionFS_t_strcat(dst, src)          lib_strcat((char*)dst,(const char*)src)
#define ionFS_t_strcmp(s1, s2)            lib_strcmp((char*)s1,(char*)s2)
#define ionFS_t_strlen(s)                 lib_strlen((const char *)s)
#define ionFS_t_strchr(str, c)            lib_strchr((const char *)(str),(const char)(c))
#define ionFS_t_strrchr(str, c)           lib_strrchr((const char *)(str),(const char)(c))
#endif

#define ionFS_t_stricmp(s1, s2)           lib_stricmp((char*)s1,(char*)s2)
#define ionFS_t_strnchr(str, c, n)        lib_strnchr((const char *)(str),(const char)(c), (int)(n))
#define ionFS_t_lstrcpy(dst, src)         lib_lstrcpy((char*)dst, (char*)src)
#define ionFS_t_lstrncpy(dst, src, len)   lib_lstrncpy((char *)dst,(const char *)src, len)
#define ionFS_t_toupper(c)                ionFS_toupper(c)
#define ionFS_t_islower(c)                ionFS_islower(c)
#define ionFS_t_isupper(c)                ionFS_isupper(c)
#define ionFS_t_isdigit(c)                ionFS_isdigit(c)

#endif

#if defined (__cplusplus )
}
#endif

#endif

/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/

