/* FILE: ion_osd.h */
/**************************************************************************
* Copyright (C)2009 Spansion LLC and its licensors. All Rights Reserved. 
*
* This software is owned by Spansion or its licensors and published by: 
* Spansion LLC, 915 DeGuigne Dr. Sunnyvale, CA  94088-3453 ("Spansion").
*
* BY DOWNLOADING, INSTALLING OR USING THIS SOFTWARE, YOU AGREE TO BE BOUND 
* BY ALL THE TERMS AND CONDITIONS OF THIS AGREEMENT.
*
* This software constitutes source code for use in programming Spansion's Flash 
* memory components. This software is licensed by Spansion to be adapted only 
* for use in systems utilizing Spansion's Flash memories. Spansion is not be 
* responsible for misuse or illegal use of this software for devices not 
* supported herein.  Spansion is providing this source code "AS IS" and will 
* not be responsible for issues arising from incorrect user implementation 
* of the source code herein.  
*
* SPANSION MAKES NO WARRANTY, EXPRESS OR IMPLIED, ARISING BY LAW OR OTHERWISE, 
* REGARDING THE SOFTWARE, ITS PERFORMANCE OR SUITABILITY FOR YOUR INTENDED 
* USE, INCLUDING, WITHOUT LIMITATION, NO IMPLIED WARRANTY OF MERCHANTABILITY, 
* FITNESS FOR A  PARTICULAR PURPOSE OR USE, OR NONINFRINGEMENT.  SPANSION WILL 
* HAVE NO LIABILITY (WHETHER IN CONTRACT, WARRANTY, TORT, NEGLIGENCE OR 
* OTHERWISE) FOR ANY DAMAGES ARISING FROM USE OR INABILITY TO USE THE SOFTWARE, 
* INCLUDING, WITHOUT LIMITATION, ANY DIRECT, INDIRECT, INCIDENTAL, 
* SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS OF DATA, SAVINGS OR PROFITS, 
* EVEN IF SPANSION HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.  
*
* This software may be replicated in part or whole for the licensed use, 
* with the restriction that this Copyright notice must be included with 
* this software, whether used in part or whole, at all times.  
*/


#if !defined( IONFS_OSD_H_05122005 )
#define IONFS_OSD_H_05122005

/*-----------------------------------------------------------------------------
 INCLUDE HEADER FILES
-----------------------------------------------------------------------------*/

#include "../ionfs.h"




/*-----------------------------------------------------------------------------
 DEFINE STRUCTURES
-----------------------------------------------------------------------------*/

/* The structure for representing current time. */
typedef struct os_tm_s {
   int32_t sec,
           min,
           hour,
           mday,
           mon,
           year,
           wday,
           yday,
           isdst;

} os_tm_t;




/*-----------------------------------------------------------------------------
 DEFINE DEFINITIONS
-----------------------------------------------------------------------------*/

/* define semaphore */
#define SM_IONFS  -1
#define SM_FAT    (SM_IONFS+1)
#define SM_OFILE  (SM_FAT+1)
#define SM_DPATH  (SM_OFILE+1)
#define SM_DCACHE (SM_DPATH+1)
#define SM_MAX    (SM_DCACHE+1)




/*-----------------------------------------------------------------------------
 DEFINE GLOBAL VARIABLES
-----------------------------------------------------------------------------*/

extern void *os_fs_sm_ptr[SM_MAX];




/*-----------------------------------------------------------------------------
 DECLARE FUNCTION PROTO-TYPE
-----------------------------------------------------------------------------*/

#if defined (__cplusplus )
extern "C" {
#endif

void os_zinit_osd( void );

/* Funtions related to Semaphore. */
int32_t os_init_sm( void );
int32_t os_terminate_sm( void );
int32_t os_obtain_sm( void *sem );
int32_t os_release_sm( void *sem );

/* Funtions related to Output. */
bool_t os_set_safe_mode( bool_t is_safe );

/* Funtions related to Time */
uint32_t FSTimeStamp( void );
os_tm_t *os_localtime( void );

void os_break( void );
void os_assert( bool_t condition );
void os_exit( uint32_t exit_code );

int32_t os_set_errno( int32_t err_no );
int32_t os_get_errno( void );

#if defined (__cplusplus )
}
#endif




/*-----------------------------------------------------------------------------
 DEFINE DEFINITIONS
-----------------------------------------------------------------------------*/

#if ( IONFS_OS == IONFS_OS_NONE )
/* Define functions related to OS. */
#define ionFS_lock()          0
#define ionFS_unlock()        0

#define ionFS_fat_lock()
#define ionFS_fat_unlock()

#define ionFS_ofile_lock()
#define ionFS_ofile_unlock()

#define ionFS_path_lock()
#define ionFS_path_unlock()

#define ionFS_cache_lock()
#define ionFS_cache_unlock()

#else

/* Define functions related to OS. */
#define ionFS_lock()          0
#define ionFS_unlock()        0

#define ionFS_fat_lock()      os_obtain_sm( os_fs_sm_ptr[SM_FAT] )
#define ionFS_fat_unlock()    os_release_sm( os_fs_sm_ptr[SM_FAT] )

#define ionFS_ofile_lock()    os_obtain_sm( os_fs_sm_ptr[SM_OFILE] )
#define ionFS_ofile_unlock()  os_release_sm( os_fs_sm_ptr[SM_OFILE] )

#define ionFS_path_lock()     os_obtain_sm( os_fs_sm_ptr[SM_DPATH] )
#define ionFS_path_unlock()   os_release_sm( os_fs_sm_ptr[SM_DPATH] )

#define ionFS_cache_lock()     os_obtain_sm( os_fs_sm_ptr[SM_DCACHE] )
#define ionFS_cache_unlock()   os_release_sm( os_fs_sm_ptr[SM_DCACHE] )
#endif

#endif

/*-----------------------------------------------------------------------------
 END OF FILE
-----------------------------------------------------------------------------*/

