#include "TIMO.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>


bool verbose = false;					// Initially verbose mode is off



/*
 * Terminal Task - Takes input command to do specific task
 * 
 * 
*/
void 
TerminalTask(void const *params)
{
	int i;

	while(1) 
	{
		i = 0;
		int c;
		char str[256];
		printf("BB> ");
		do
		{
			c = fgetc(stdin);
			if(c=='\n')
				break;

			if(c!=-1)
			{
				str[i++] = c;
			}
			delay(10);
		}while(1);
		str[i]='\0';
		//printf("Got..%s\n",str);
		//char s2[20];
		//strcpy(s2,"hello");
		if(str[0]=='v' && str[1]==' ' && str[2]=='1')
		{
			verbose = true;
		}
		else if(str[0]=='v' && str[1]==' ' && str[2]=='0')
		{
			verbose = false;
		}
		else if(str[0]=='s' && str[1]=='y' && str[2]=='s')
		{
			printf("--------------BlueBrain System Info------------------\n");
			printf("ADC: 4 channels\n");
			printf("GPIO: 8 configurable to input switch and output LED\n");
			printf("Supporting on chip file system with 16M bytes memory\n");
			printf("I2C: User I2C bus to connect external sensors\n");
			printf("I2C: Sensor I2C bus connected internally to Accelerometer and Temperature sensor\n");
			delay(10);	// Put delay to give some time to empty TX buffer
			printf("Accelerometer sensor: Configurable upto +/- 16G, 12 bit output, with 3 different power modes\n");
			printf("Temperature sensor: Accuracy +/- 0.5 degree Centigrade\n");
			printf("CAN bus: user configurable\n");
		}
		else if(str[0]=='h' && str[1]=='e' && str[2]=='l' && str[3]=='p')
		{
			printf("Verbose mode: v <1/0>on/off \n");
		}
	}
}

	