#include "TIMO.h"
#include "main.h"
#include "cmsis_os.h"
#include "RS_485.h"
#include <time.h>

#define SENSOR_BOARD_ADDRESS 0x1F  // only one sensor board connected now (no K3 jumper populated)


/*
 *
 * User Uart Task
 *
 *
 */
static timoPacket 
  txPacket;

static uint8_t rxCounter;

static RTC_TimeTypeDef curr_time_stamp;
static RTC_DateTypeDef curr_date_stamp;

void UserUartTask(void const *params)
{
	uint8_t sec_old;
	uint8_t read_time_state;
	uint32_t txpacket_duration;

	//sprintf(buf, "TIMO Coordinator Board\r\n");
	//USER_UART.println(buf);

	
	delay(1000);
	
  #ifdef JITTER_TEST
	
	for(;;)
	{
	
		//
		// send round-trip character to Sensor Board, assert GPIO_7 for 200 mSec
		GPIO_set(GPIO_7);
		RS_485_sendAddressCharacter(RS_485_ROUND_TRIP + SENSOR_BOARD_ADDRESS); // Send Round-Trip Address character		
		delay(200);
		GPIO_reset(GPIO_7);
		
		delay (300);
	}
  #endif // #ifdef JITTER_TEST
	
	
	
	
	
	
	
	#ifdef SYNC_TEST	
	// Sync test
	txPacket.address = RS_485_SET_TIME + SENSOR_BOARD_ADDRESS; //Send SET TIME address character	
	
	printf("txPacket.address: %02X\n", txPacket.address);
	//
	// duration of TIMO packet =
	//
	//  [(11 bits/char) * chars/packet * 1000000 uS/Sec] / (115200 bits/sec)  = 955 uS/packet
	txpacket_duration = 11 * (sizeof(timoPacket) * 1000000) / 115200;

  printf("txpacket_duration: %d\n", txpacket_duration);
	
	//
	// Read time, waiting for the seconds to change
	//
  for (read_time_state = 0;;)
	{
		//
	  // send the packet one duration-of-TIMO-packet before new second
	  // It will then arrive at the Sensor Board on-the-second.
	  //

	  HAL_RTC_GetTime(&hrtc,&curr_time_stamp,RTC_FORMAT_BIN);
	  HAL_RTC_GetDate(&hrtc,&curr_date_stamp,RTC_FORMAT_BIN);
		
		switch(read_time_state)
		{
			case 0:
		    if (curr_time_stamp.SubSeconds > (txpacket_duration/30))
				{
	  			read_time_state = 1;			
				}
			  break;
				
			case 1:
			  if (curr_time_stamp.SubSeconds <= (txpacket_duration/30))
			  {			
          txPacket.hms   = curr_time_stamp.Hours   *(60*60) +
					  		          (curr_time_stamp.Minutes *60) +
					  	             curr_time_stamp.Seconds;			
	        txPacket.sub_seconds = 0;
	
	        //
	        // add 1 second to target time
					//
	        txPacket.hms++;
	        txPacket.hms %= (24*60*60);	
		
	        RS_485_sendPacket(txPacket);
 #if 0
					printf("set hms: %02d:%02d:%02d.%05d\n", 
					  curr_time_stamp.Hours,
					  curr_time_stamp.Minutes,
					  curr_time_stamp.Seconds,
					  curr_time_stamp.SubSeconds
					);
#endif					
  //        printf("RS_485_sendPacket(txPacket)\n");			
					read_time_state = 2;
			  }
			  break;		

			case 2:
		    delay(4000);	
        read_time_state = 0;			
        break;			
	  }	// end switch(read_time_state)
	} // end for (read_time_state = 0;;)
	#endif // #ifdef SYNC_TEST
	
	
} // end static void UserUartTask(void const *params)

#ifdef SYNC_TEST
void RTCMonTask(void const *params)
{
  RTC_TimeTypeDef temp_time_stamp;
  RTC_DateTypeDef temp_date_stamp;
	uint8_t prev_sec;
	
	/* update GPIO_6 to reflect current second: 
      odd second: 1
     even second: 0
	*/
			
	for(;;)
	{
	  HAL_RTC_GetTime(&hrtc,&temp_time_stamp,RTC_FORMAT_BIN);
	  HAL_RTC_GetDate(&hrtc,&temp_date_stamp,RTC_FORMAT_BIN);

		if (prev_sec != temp_time_stamp.Seconds)
	  {
	  	if (temp_time_stamp.Seconds & 0x01)
	  		GPIO_reset(GPIO_6);
	  	else
	  		GPIO_set(GPIO_6);
		  prev_sec = temp_time_stamp.Seconds;
	  } // end if (prev_sec != temp_time_stamp.Seconds)
	}	// end for(rtc_mon_state = 0;;)	
}
#endif

