/**
  ******************************************************************************
  * File Name          : mxconstants.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * COPYRIGHT(c) 2015 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define EVENT_OUTPUT_Pin GPIO_PIN_13
#define EVENT_OUTPUT_GPIO_Port GPIOC
#define USER_GPIO0_0_Pin GPIO_PIN_0
#define USER_GPIO0_0_GPIO_Port GPIOC
#define USER_GPIO0_1_Pin GPIO_PIN_1
#define USER_GPIO0_1_GPIO_Port GPIOC
#define USER_GPIO0_2_Pin GPIO_PIN_2
#define USER_GPIO0_2_GPIO_Port GPIOC
#define USER_GPIO0_3_Pin GPIO_PIN_3
#define USER_GPIO0_3_GPIO_Port GPIOC
#define WAKEUP_INPUT_Pin GPIO_PIN_0
#define WAKEUP_INPUT_GPIO_Port GPIOA
#define USER_PWM_OUTPUT_Pin GPIO_PIN_1
#define USER_PWM_OUTPUT_GPIO_Port GPIOA
#define USER_UART_TX_Pin GPIO_PIN_2
#define USER_UART_TX_GPIO_Port GPIOA
#define USER_UART_RX_Pin GPIO_PIN_3
#define USER_UART_RX_GPIO_Port GPIOA
#define USER_A2_Pin GPIO_PIN_4
#define USER_A2_GPIO_Port GPIOA
#define USER_A3_Pin GPIO_PIN_5
#define USER_A3_GPIO_Port GPIOA
#define USER_A0_Pin GPIO_PIN_6
#define USER_A0_GPIO_Port GPIOA
#define USER_A1_Pin GPIO_PIN_7
#define USER_A1_GPIO_Port GPIOA
#define USER_GPIO_4_Pin GPIO_PIN_4
#define USER_GPIO_4_GPIO_Port GPIOC
#define USER_GPIO_5_Pin GPIO_PIN_5
#define USER_GPIO_5_GPIO_Port GPIOC
#define LED_R_Pin GPIO_PIN_0
#define LED_R_GPIO_Port GPIOB
#define LED_G_Pin GPIO_PIN_1
#define LED_G_GPIO_Port GPIOB
#define LED_B_Pin GPIO_PIN_2
#define LED_B_GPIO_Port GPIOB
#define USER_I2C_SCL_Pin GPIO_PIN_10
#define USER_I2C_SCL_GPIO_Port GPIOB
#define USER_I2C_SDA_Pin GPIO_PIN_11
#define USER_I2C_SDA_GPIO_Port GPIOB
#define USER_CAN_RX_Pin GPIO_PIN_12
#define USER_CAN_RX_GPIO_Port GPIOB
#define FLASH_SCK_Pin GPIO_PIN_13
#define FLASH_SCK_GPIO_Port GPIOB
#define FLASH_MISO_Pin GPIO_PIN_14
#define FLASH_MISO_GPIO_Port GPIOB
#define FLASH_MOSI_Pin GPIO_PIN_15
#define FLASH_MOSI_GPIO_Port GPIOB
#define USER_GPIO_6_Pin GPIO_PIN_6
#define USER_GPIO_6_GPIO_Port GPIOC
#define USER_GPIO_7_Pin GPIO_PIN_7
#define USER_GPIO_7_GPIO_Port GPIOC
#define BLE_LINK_HS_IN_Pin GPIO_PIN_8
#define BLE_LINK_HS_IN_GPIO_Port GPIOC
#define BLE_LINK_HS_OUT_Pin GPIO_PIN_9
#define BLE_LINK_HS_OUT_GPIO_Port GPIOC
#define LOCAL_PUSHBUTTON_Pin GPIO_PIN_8
#define LOCAL_PUSHBUTTON_GPIO_Port GPIOA
#define BLE_UART_TX_Pin GPIO_PIN_9
#define BLE_UART_TX_GPIO_Port GPIOA
#define BLE_UART_RX_Pin GPIO_PIN_10
#define BLE_UART_RX_GPIO_Port GPIOA
#define BLE_UART_CTS_Pin GPIO_PIN_11
#define BLE_UART_CTS_GPIO_Port GPIOA
#define BLE_UART_RTS_Pin GPIO_PIN_12
#define BLE_UART_RTS_GPIO_Port GPIOA
#define DEBUG_SWDIO_Pin GPIO_PIN_13
#define DEBUG_SWDIO_GPIO_Port GPIOA
#define DEBUG_SWCLK_Pin GPIO_PIN_14
#define DEBUG_SWCLK_GPIO_Port GPIOA
#define USER_SPI_CS_Pin GPIO_PIN_15
#define USER_SPI_CS_GPIO_Port GPIOA
#define DEBUG_UART_TX_Pin GPIO_PIN_10
#define DEBUG_UART_TX_GPIO_Port GPIOC
#define DEBUG_UART_RX_Pin GPIO_PIN_11
#define DEBUG_UART_RX_GPIO_Port GPIOC
#define SENSOR_INT_Pin GPIO_PIN_12
#define SENSOR_INT_GPIO_Port GPIOC
#define KICK_BLE_RESET_Pin GPIO_PIN_2
#define KICK_BLE_RESET_GPIO_Port GPIOD
#define USER_SPI_SCK_Pin GPIO_PIN_3
#define USER_SPI_SCK_GPIO_Port GPIOB
#define USER_SPI_MISO_Pin GPIO_PIN_4
#define USER_SPI_MISO_GPIO_Port GPIOB
#define USER_SPI_MOSI_Pin GPIO_PIN_5
#define USER_SPI_MOSI_GPIO_Port GPIOB
#define USER_CAN_TX_Pin GPIO_PIN_6
#define USER_CAN_TX_GPIO_Port GPIOB
#define SENSOR_I2C_SDA_Pin GPIO_PIN_7
#define SENSOR_I2C_SDA_GPIO_Port GPIOB
#define SENSOR_I2C_SCL_Pin GPIO_PIN_8
#define SENSOR_I2C_SCL_GPIO_Port GPIOB
#define FLASH_CS_Pin GPIO_PIN_9
#define FLASH_CS_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
