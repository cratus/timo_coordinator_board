'''
==================================================================================================================================

 Copyright CRATUS TECHNOLOGY INC, 2015-17
 All Rights Reserved
 UNPUBLISHED, LICENSED SOFTWARE.

 CONFIDENTIAL AND PROPRIETARY INFORMATION

 WHICH IS THE PROPERTY OF CRATUS TECHNOLOGY INC.

==================================================================================================================================
 
 TIMO_SensorBoardTest.py - Test program to send commands and receive responses from TIMO Sensor Board

 Support modules:

   Uart.py

 Note: It is assumed that a TIMO Sensor Board is attached to the PC. (port 28)
==================================================================================================================================
'''
from Uart     import *   # UART routines

import sys
from  time import sleep
import msvcrt
from binascii         import hexlify

#############
#
# main loop
#
#############

#           address event   Year  Mon Day   hr, min, sec subseconds    CRC
stPacket = [0x6F,   0x00,   16,   2,  29,   23, 59,  54, 0x00, 0x00,   0,0]
gtPacket = [0x4F]
rtPacket = [0x3F,] # round-trip character

#
#   Get the COM PORT number from user and open a connection
#
serialPortHandle = connectToXPS();



#
# Send round-trip address byte to connected TIMO Sensor Board
#
serialPortHandle.write(rtPacket)
print "rtPacket"
print rtPacket

#
# Read back echoed round-trip byte
#
packet = serialPortHandle.read(30)
str = hexlify(packet)
print str.upper()
sleep(1)



#
# send set-time packet to Sensor Board
#
serialPortHandle.write(stPacket)
print stPacket

#
# Read back 
#


packet = serialPortHandle.read(30)
str = hexlify(packet)

print "read back set_time----\n"
print str.upper()
print packet
print "-----\n"


sleep(1)

while(1):
  #
  # Send get-time address byte
  #
  print "get-time address byte sent\n"

  serialPortHandle.write(gtPacket)

  #
  # Read back time packete from Sensor Board
  #
  packet = serialPortHandle.read(30)
  str = hexlify(packet)
  print str.upper()
  sleep(.1)

#################
#
# end main loop
#
#################



