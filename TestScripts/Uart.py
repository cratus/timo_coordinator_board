'''
==================================================================================================================================

 Copyright CRATUS TECHNOLOGY INC, 2015-16
 All Rights Reserved
 UNPUBLISHED, LICENSED SOFTWARE.

 CONFIDENTIAL AND PROPRIETARY INFORMATION

 WHICH IS THE PROPERTY OF CRATUS TECHNOLOGY INC.

==================================================================================================================================
 
 Support module for XPS test suite - main module: XPS_IMS_Test.py

 XPS_IMS_Uart.py - UART routines

==================================================================================================================================
'''

import serial

#
# UART interface
#

def connectToXPS():
    COMPort = 'COM28'
    print('Connecting to TIMO Sensor Board on ' + COMPort + ' ........')

    #   Open serial port
    ser = serial.Serial(COMPort, baudrate=9600, timeout=0.200) # timeout = 200 mSec
    print('Connected')

    return ser

def sendCommandPacket(ser, cmd_packet):
    #  Write command packet to serial port
    ser.write(cmd_packet);


def getResponsePacket(ser):
    return ser.read(3000)
