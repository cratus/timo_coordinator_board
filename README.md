# Progress Status of the project #

## In Progress ##
* Test CPU_RESET and BLE_RESET functionality.

## Pending Tasks ##

* Write BB HAL common module for the CAN peripheral.
* Test [Wakeup CPU](https://drive.google.com/open?id=1LTGxGAJF8Et6JNIYoXUUmBq_zCPNs5XHW49hqmge_48) functionality from BLE and from configuring RTC Alarm.
* Implement [FOTA](https://drive.google.com/open?id=1bUaTKi91sPnvlZr2PPU0L4WwrFbM2ZOSxAx9T7JGTLI) functionality.
* Make Logger api to log general data into a file
* Implement watchdog task to detect if program is trapped somewhere for long duration.
* Stack Overflow detection and log that data into file and when program is restarting again it should show the task which is breaking stack.
* Implement bootloader code and if system failure detected then stay in bootloader mode and wait for firmware update over the air.
* Make interrupt based IO drivers.
* Implement DMA to transfer chunks of data to the peripheral and from the peripheral.
* Make RTOS wrapper functions to add any new task 

## Finished Tasks ##

* Check BOOT0/BOOT1 functionalities to do firmware update over the serial connection. (In memory [UART bootloader](http://stm32f4-discovery.net/2014/09/program-stm32f4-with-uart/)).
* CAN bus testing - Code needs to be commited.
* Tested OK all IO peripherals.
* Spansion File System tested OK without any operating system
* Implemented RTX os with C++ support 
* Added terminal support
* Implemented reading and writing file functionality using python script
* Implemented internal watchdog timer(Independent Watchdog timer)             
* Implemented Spansion Flash File System and testing with RTOS
* RTC super capacitor sustain charge for atleast 24 hours.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Refer [this](http://www2.st.com/content/st_com/en/products/embedded-software/mcus-embedded-software/stm32-embedded-software/stm32cube-embedded-software/stm32cubef4.html) to download STM32CubeF4 Software package to get sample projects.
* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* If you have any further questions related to this project then contact on this email : dhruv@cratustech.com